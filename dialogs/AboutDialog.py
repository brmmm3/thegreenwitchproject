# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from typing import TYPE_CHECKING

from PySide6.QtCore import Slot, Qt
from PySide6.QtWidgets import QDialog

from PySide6_loadUi import loadUi

#from common import HELP_URL, ScaleGui, OpenUrl
from dialogs.ChangelogDialog import ChangelogDialog

if TYPE_CHECKING:
    from greenwitch import MainWindow


# noinspection PyCallingNonCallable
class AboutDialog(QDialog):

    def __init__(self, app: 'MainWindow'):
        # noinspection PyArgumentList
        super().__init__()
        self.app: 'MainWindow' = app
        loadUi(":ui/AboutDialog.ui", self)
        self.setWindowFlag(Qt.WindowContextHelpButtonHint, False)

    @Slot()
    def on_btnChangelog_released(self):
        dlg = ChangelogDialog(self.app)
        dlg.exec_()

    @Slot()
    def on_btnClose_released(self):
        self.hide()
