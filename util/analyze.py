import io
from datetime import datetime
from typing import List, Dict

import branca
import folium

from PySide6.QtWebEngineWidgets import QWebEngineView
from PySide6.QtWidgets import QDialog, QVBoxLayout

from multisensor import BinFile

from util.common import CreatePlot, AddTitleLabel
from util.sensordata import getGPSBoundingRect, LatLngAltType, getGPSPath


class MapWindow(QDialog):

    def __init__(self, title: str, file: BinFile, yValues: dict, colorRef: str | None = None, parent=None):
        super().__init__(parent)
        self.setWindowTitle(title)
        vLayout = QVBoxLayout()
        view = QWebEngineView()
        vLayout.addWidget(view)
        self.setLayout(vLayout)
        self.gpsPath: Dict[datetime, LatLngAltType] = {pos.date_time: pos
                                                       for pos in getGPSPath([(data.date_time, data.pa1010d) for data in file.data])}
        gpsTimeline = sorted(self.gpsPath)
        curTime = gpsTimeline[0]
        for t in gpsTimeline:
            if t > curTime:
                break
            curTime = t
        pos = self.gpsPath.get(curTime)
        lat, lng, dLat, dLng = getGPSBoundingRect(list(self.gpsPath.values()))
        analyzeMap = folium.Map(
            location=[lat + 0.5 * dLat, lng + 0.5 * dLng],
            tiles="Stamen Terrain",
            zoom_start=13
        )
        # Add (colored) track
        if colorRef:
            y = yValues.get(colorRef)
            if None in y:
                y0 = [d for d in y if d is not None]
                if y0:
                    y0 = sum(y0) / len(y0)
                    y = [d if d is not None else y0 for d in y]
                else:
                    y = None
            if y:
                colormap = branca.colormap.linear.YlOrRd_09.scale(min(y), max(y)).to_step(10)
                route = [(pos.lat, pos.lng) for k, pos in sorted(self.gpsPath.items())]
                folium.ColorLine(positions=route, colormap=colormap, weight=10, colors=y).add_to(analyzeMap)
                analyzeMap.add_child(colormap)
        else:
            folium.PolyLine([(pos.lat, pos.lng) for pos in self.gpsPath.values()]).add_to(analyzeMap)
        # Add current position marker
        analyzePosMarker = folium.Marker([pos.lat, pos.lng], popup="Current Position", tooltip=f"Satellites {pos.satellites}")
        analyzePosMarker.add_to(analyzeMap)
        data = io.BytesIO()
        analyzeMap.save(data, close_file=False)
        view.setHtml(data.getvalue().decode())

    def updateCurPos(self, curTime):
        pos = self.gpsPathDict.get(curTime)
        for fig, _, _ in self.analyzeFigures.values():
            self.line.set_xdata(curTime)
            fig.draw()


class AnalyzeWindow(QDialog):

    def __init__(self, title: str, signals: List[str], file: BinFile, yValues: dict, parent=None):
        super().__init__(parent)
        self.setWindowTitle(title)
        vLayout = QVBoxLayout()
        self.setLayout(vLayout)
        self.analyzeFigures = {}
        x = [data.date_time for data in file.data]
        units = {"Temperature": "°C", "Humidity": "%", "Pressure": "hPa", "CO2": "ppm", "O2": "%", "C2H5OH": "ppm",
                 "NO2": "ppm", "VOC": "ppm", "CO": "ppm", "PM2.5": "ug/m^3", "PM10": "ug/m^3", "IMU9DOF": "uT", "TLV493D": "mT"}
        for signal in signals:
            unit = units.get(signal)
            y = yValues.get(signal)
            if not y:
                continue
            fig, sub = CreatePlot(unit, x, y)
            fig.setMinimumHeight(300)
            self.line = sub.axvline(x=x[0], color="r")
            vLayout.addWidget(AddTitleLabel(f"{signal} [{unit}]"))
            vLayout.addWidget(fig)
            self.analyzeFigures[signal] = (fig, sub, y)

    def updateCurPos(self, curTime):
        for fig, _, _ in self.analyzeFigures.values():
            self.line.set_xdata(curTime)
            fig.draw()
