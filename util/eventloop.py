# -*- coding: utf-8 -*-
# Author: Martin Bammer
# eMail: Martin.Bammer2@at.bosch.com
import asyncio
import sys
import traceback

# noinspection PyProtectedMember
from qasync import QEventLoop, QApplication


def GetEventLoop():
    try:
        return asyncio.get_event_loop()
    except RuntimeError:
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        return loop


# noinspection PyMethodMayBeStatic
class QEventLoopStackTraced(QEventLoop):

    def __init__(self, application: QApplication):
        super().__init__(application)

    def call_later(self, delay: float, fn, *args, context=None):
        """Submits the wrapped function instead of `fn`"""
        return super().call_later(delay, self._function_wrapper, fn, *args, context=context)

    def call_soon(self, fn, *args, context=None):
        """Submits the wrapped function instead of `fn`"""
        return super().call_soon(self._function_wrapper, fn, *args, context=context)

    def _function_wrapper(self, fn, *args):
        """Wraps `fn` in order to preserve the traceback of any kind of raised exception"""
        try:
            return fn(*args)
        except Exception:
            raise sys.exc_info()[0](traceback.format_exc())
