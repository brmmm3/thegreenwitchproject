import os
import sys

from multisensor import merge_bin_files


if __name__ == "__main__":
    dirName = sys.argv[1]
    records, errors = merge_bin_files(os.listdir(dirName))
    for pathName, error in errors.items():
        print(f"Failed to load {pathName} (Size={os.path.getsize(pathName)}):")
        print(error)
    print(f"Found {len(records)} records.")
    for nr, record in enumerate(records):
        print(f"Record {nr + 1:3}: {record.cnt:4} data points. Start @ {record.start}."
              f" Duration is {record.duration}. GPS: {record.has_gps_str()}")
