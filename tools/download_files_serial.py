import os
import sys
import traceback
from datetime import date
from typing import List

import serial

import multisensor
from serial import Serial


def GetFileList() -> List[str]:
    with serial.Serial('/dev/ttyUSB0', 230400, timeout=1) as S:
        S.write(b'l')
        fileNames = []
        while True:
            line = S.readline()
            if not line:
                break
            if line.startswith(b"::OK"):
                break
            if line.startswith(b"::"):
                fileName = line[3:].strip().decode("utf-8")
                fileNames.append(fileName)
            else:
                print(line)
    return fileNames


def RemoveLocalFile(dirName: str, fileName: str):
    os.remove(f"{dirName}/{fileName}")
    if not os.listdir(dirName):
        print(f"Remove empty directory {dirName}")
        os.rmdir(dirName)


def RemoveRemoteFile(ser: Serial, fileName: str, nr: int, fileCnt: int):
    print(f"Delete {nr + 1}/{fileCnt} {fileName}...")
    ser.write(b"F/" + fileName.encode("utf-8"))


def RemoveFile(ser: Serial, dirName: str, fileName: str, nr: int, fileCnt: int):
    RemoveLocalFile(dirName, fileName)
    RemoveRemoteFile(ser, fileName, nr, fileCnt)


def DownloadFile(S: Serial, fileName: str) -> bytes | None:
    S.reset_input_buffer()
    S.write(b'f/' + fileName.encode("utf-8"))
    binData = b""
    n = 0
    while True:
        chunk = S.readline().rstrip()
        if not chunk or chunk == b"::OK":
            break
        if chunk.startswith(b"::"):
            binData += chunk[2:]
            n += 1
        elif not chunk.startswith(b"I:"):
            print(chunk)
        if chunk.startswith(b"E:File not found!"):
            return None
    fileData = binData.decode("utf-8")
    return bytes([int(x[0] + x[1], 16) for x in zip(fileData[0::2], fileData[1::2])])


def DownloadFiles(dirName: str, remoteFileNames: List[str]):
    with serial.Serial('/dev/ttyUSB0', 230400, timeout=3) as S:
        fileCnt = len(remoteFileNames)
        for nr, fileName in enumerate(remoteFileNames):
            bRemove = True
            for _ in range(3):
                print(f"Download {nr + 1}/{len(remoteFileNames)} {fileName}...")
                try:
                    fileData = DownloadFile(S, fileName)
                    if fileData is None:
                        bRemove = False
                        break
                    print(f"Write {len(fileData)} bytes to file {dirName}/{fileName}")
                    with open(f"{dirName}/{fileName}", 'wb') as F:
                        F.write(fileData)
                    multisensor.load_bin_file(f"{dirName}/{fileName}")
                    break
                except TimeoutError:
                    print(f"Timeout! Failed to receive file! Remove file {fileName} and abort.")
                    RemoveLocalFile(dirName, fileName)
                    return
                except Exception as exc:
                    errMsg = str(exc)
                    if "too big" in errMsg:
                        if "ValueError: file_header.data_cnt" in errMsg:
                            RemoveFile(S, dirName, fileName, nr, fileCnt)
                            bRemove = False
                            break
                    traceback.print_exc()
                    if "incomplete" not in errMsg and "Failed to read whole file" not in errMsg:
                        file = multisensor.load_bin_file(f"{dirName}/{fileName}", force=True)
                        print(file)
                        break
            if bRemove:
                RemoveRemoteFile(S, fileName, nr, fileCnt)


if __name__ == "__main__":
    if "--files" in sys.argv:
        remoteFileNames = sys.argv[sys.argv.index("--files") + 1].split(",")
    else:
        remoteFileNames = None
    if remoteFileNames is None:
        print("Get SD-Card contents...")
        remoteFileNames = GetFileList()
        if not remoteFileNames:
            print("No files to download.")
            sys.exit(0)
    dataDirName = "data" if os.path.exists("data") else "../data"
    today = date.today()
    dirNameBase = today.strftime(f"{dataDirName}/%Y.%m.%d")
    dirName = dirNameBase
    # Only choose new directory if local files intersect with remote files
    if os.path.exists(dirNameBase) and set(os.listdir(dirNameBase)).intersection(remoteFileNames):
        nr = 1
        while os.path.exists(dirName):
            nr += 1
            dirName = f"{dirNameBase}-{nr}"
    print(f"Download {len(remoteFileNames)} data files to {dirName}...")
    if not os.path.exists(dirName):
        os.mkdir(dirName)
    DownloadFiles(dirName, remoteFileNames)
    print("Finished.")
