import sys

def getIP(default = "192.168.1.17"):
    if "--ip" not in sys.argv:
        return default
    return sys.argv[sys.argv.index("--ip") + 1]
