import json
import os
import sys
import traceback
import asyncio
from datetime import date
from typing import List

import websockets
from ftplib import FTP

from multisensor import BinFile, load_bin_file

from default_ip import getIP


async def GetFileList(ip: str) -> List[str]:
    print("Get SD-Card contents...")
    async with websockets.connect(f"ws://{ip}/ws") as ws:
        await ws.recv()
        offset = 0
        allFileNames = []
        while True:
            await ws.send(f"list_sdcard_files {offset}")
            result = await ws.recv()
            if not result:
                break
            data = json.loads(result)
            if data.get("filename_nr") == -1:
                break
            fileNames = data.get("filenames")
            if fileNames:
                fileNames = [fileName[1:] for fileName in fileNames.splitlines()]
                offset += len(fileNames)
                allFileNames.extend(fileNames)
                print(f"{offset} files")
    return allFileNames


def RemoveLocalFile(dirName: str, fileName: str):
    os.remove(f"{dirName}/{fileName}")
    if not os.listdir(dirName):
        print(f"Remove empty directory {dirName}")
        os.rmdir(dirName)


def RemoveRemoteFile(fileName: str, nr: int, fileCnt: int):
    print(f"Delete {nr + 1}/{fileCnt} {fileName}...")
    ftp.delete(fileName)


def RemoveFile(dirName: str, fileName: str, nr: int, fileCnt: int):
    RemoveLocalFile(dirName, fileName)
    RemoveRemoteFile(fileName, nr, fileCnt)


def DownloadFiles(dirName: str, remoteFileNames: List[str], keep: bool):
    fileCnt = len(remoteFileNames)
    failed = {}
    for nr, fileName in enumerate(remoteFileNames):
        if not fileName.startswith("B") or not fileName.endswith(".bin"):
            continue
        for _ in range(3):
            print(f"Download {nr + 1}/{len(remoteFileNames)} {fileName}... ", end="")
            try:
                with open(f"{dirName}/{fileName}", 'wb') as F:
                    ftp.retrbinary(f"RETR {fileName}", F.write)
                file: BinFile = load_bin_file(f"{dirName}/{fileName}")
                print(f"Version={file.file_header.data_version} DataCnt={file.file_header.data_cnt}")
                break
            except TimeoutError:
                print(f"\nTimeout! Failed to receive file! Remove file {fileName} and abort.")
                if not keep:
                    RemoveLocalFile(dirName, fileName)
                return
            except Exception as exc:
                print("")
                errMsg = str(exc)
                if "too big" in errMsg:
                    if "ValueError: file_header.data_cnt" in errMsg:
                        failed[fileName] = errMsg
                        if not keep:
                            RemoveFile(dirName, fileName, nr, fileCnt)
                        continue
                traceback.print_exc()
                if "incomplete" not in errMsg and "Failed to read whole file" not in errMsg:
                    failed[fileName] = errMsg
                    break
        if not keep:
            RemoveRemoteFile(fileName, nr, fileCnt)
    if failed:
        print(f"Failed to download{len(failed)} files!")


if __name__ == "__main__":
    keep = "--keep" in sys.argv
    skip = "--skip" in sys.argv
    if "--files" in sys.argv:
        remoteFileNames = sys.argv[sys.argv.index("--files") + 1].split(",")
    else:
        remoteFileNames = None
    dataDirName = "data" if os.path.exists("data") else "../data"
    today = date.today()
    dirNameBase = today.strftime(f"{dataDirName}/%Y.%m.%d")
    dirName = dirNameBase
    ip = getIP()
    print(f"IP: {ip}")
    if not remoteFileNames:
        remoteFileNames = asyncio.run(GetFileList(ip))
    if skip and os.path.exists(dirNameBase):
        remoteFileNames = sorted(set(remoteFileNames).difference(os.listdir(dirNameBase)))
    if not remoteFileNames:
        print("No files to download.")
        sys.exit(0)
    with FTP(ip, timeout=10) as ftp:
        print("Login...")
        ftp.login("MS01", "MS01")
        # Only choose new directory if local files intersect with remote files
        if os.path.exists(dirNameBase) and set(os.listdir(dirNameBase)).intersection(remoteFileNames):
            nr = 1
            while os.path.exists(dirName):
                nr += 1
                dirName = f"{dirNameBase}-{nr}"
        print(f"Download {len(remoteFileNames)} data files to {dirName}...")
        if not os.path.exists(dirName):
            os.mkdir(dirName)
        DownloadFiles(dirName, remoteFileNames, keep)
    print("Finished.")
