import sys
import subprocess

# First run: ~/esp/esp-idf/export.sh
# Call: decode_backtrace.ps /tmp/arduino-sketch-29DD3EC310F7DB914845029904F1914A/MultiSensor.ino.elf backtrace.txt

if __name__ == "__main__":
    elfPath = sys.argv[1]
    backtracePath = sys.argv[2]
    addresses = None
    with open(backtracePath) as F:
        for line in F:
            if line.startswith("Backtrace: "):
                addresses = [x.split(":")[0] for x in line.split(" ") if ":0" in x]
                break
    if not addresses:
        print("Invalid backtrace file!")
        sys.exit(1)
    for address in addresses:
        subprocess.call(["xtensa-esp32-elf-addr2line", "-pfiaC", "-e", elfPath, address])
