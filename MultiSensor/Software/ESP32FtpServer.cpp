/*
 * FTP Serveur for ESP8266
 * based on FTP Serveur for Arduino Due and Ethernet shield (W5100) or WIZ820io (W5200)
 * based on Jean-Michel Gallego's work
 * modified to work with esp8266 SPIFFS by David Paiva david@nailbuster.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//  2017: modified by @robo8080
// 2019: modified by @fa1ke5

#include "ESP32FtpServer.h"

#include <WiFi.h>
// #include <ESP32WebServer.h>
#include <FS.h>
#include "SD.h"
// #include "SPI.h"

char ftpbuffer[4096];

WiFiServer ftpServer(FTP_CTRL_PORT);
WiFiServer dataServer(FTP_DATA_PORT_PASV);

void FtpServer::begin(String uname, String pword) {
  // Tells the ftp server to begin listening for incoming connection
  _FTP_USER = uname;
  _FTP_PASS = pword;
  ftpServer.begin();
  delay(10);
  dataServer.begin();
  delay(10);
  millisTimeOut = (uint32_t)FTP_TIME_OUT * 60 * 1000;
  millisDelay = 0;
  cmdStatus = 0;
  iniVariables();
}

void FtpServer::iniVariables() {
  // Default for data port
  dataPort = FTP_DATA_PORT_PASV;

  debugMode = false;

  // Default Data connection is Active
  dataPassiveConn = true;

  // Set the root directory
  strcpy(cwdName, "/");

  rnfrCmd = false;
  transferStatus = 0;
}

bool FtpServer::isClientConnected() {
  return cmdStatus == 5;
}

int8_t FtpServer::getTransferStatus() {
  return transferStatus;
}

const char *FtpServer::getCurrentFileName() {
  if (transferStatus == 0)
    return NULL;
  return file.path();
}

void FtpServer::handleFTP() {
  if ((int32_t)(millisDelay - millis()) > 0)
    return;
  if (ftpServer.hasClient()) {
    // if (ftpServer.available()) {
    client.stop();
    client = ftpServer.available();
  }
  // if (cmdStatus != 5) Serial.println("cmdStatus " + String(cmdStatus));
  if (cmdStatus == 0) {
    if (client.connected())
      disconnectClient();
    cmdStatus = 1;
  } else if (cmdStatus == 1) {
    // Ftp server waiting for connection
    abortTransfer();
    iniVariables();
    if (debugMode) {
      Serial.println("I:Ftp server waiting for connection on port " + String(FTP_CTRL_PORT));
    }
    cmdStatus = 2;
  } else if (cmdStatus == 2) {
    // Ftp server idle
    if (client.connected()) {
      // A client connected
      clientConnected();
      millisEndConnection = millis() + 10 * 1000; // wait client id during 10 s.
      cmdStatus = 3;
    }
  } else if (readChar() > 0) {
    // got response
    if (cmdStatus == 3)
      // Ftp server waiting for user identity
      if (userIdentity())
        cmdStatus = 4;
      else
        cmdStatus = 0;
    else if (cmdStatus == 4)
      // Ftp server waiting for user registration
      if (userPassword()) {
        cmdStatus = 5;
        millisEndConnection = millis() + millisTimeOut;
      } else
        cmdStatus = 0;
    else if (cmdStatus == 5) {
      // Ftp server waiting for user command
      if (!processCommand())
        cmdStatus = 0;
      else
        millisEndConnection = millis() + millisTimeOut;
    }
  } else if (!client.connected() || !client) {
    cmdStatus = 1;
    if (debugMode) {
      Serial.println("I:Client disconnected");
    }
  }
  // if (transferStatus) Serial.println("transferStatus " + String(transferStatus));
  if (transferStatus == 1) {
    // Retrieve data
    if (!doRetrieve()) {
      transferStatus = 0;
      yield();
      delay(1);
    }
  } else if (transferStatus == 2) {
    // Store data
    if (!doStore())
      transferStatus = 0;
  } else if (cmdStatus > 2 && !((int32_t)(millisEndConnection - millis()) > 0)) {
    client.println("530 Timeout");
    // delay of 200 ms
    millisDelay = millis() + 200;
    cmdStatus = 0;
  }
}

void FtpServer::clientConnected() {
  if (debugMode) {
    Serial.println("I:Client connected!");
  }
  client.println("220--- Welcome to FTP for ESP8266 ---");
  client.println("220---   By David Paiva   ---");
  sprintf(ftpbuffer, "220 --   Version %s   --", FTP_SERVER_VERSION);
  client.println(ftpbuffer);
  iCL = 0;
}

void FtpServer::disconnectClient() {
  if (debugMode) {
    Serial.println("I:Disconnecting client");
  }
  abortTransfer();
  client.println("221 Goodbye");
  client.stop();
}

boolean FtpServer::userIdentity() {
  if (strcmp(command, "USER"))
    client.println("500 Syntax error");
  if (strcmp(parameters, _FTP_USER.c_str()))
    client.println("530 user not found");
  else {
    client.println("331 OK. Password required");
    strcpy(cwdName, "/");
    return true;
  }
  millisDelay = millis() + 100; // delay of 100 ms
  return false;
}

boolean FtpServer::userPassword() {
  if (strcmp(command, "PASS"))
    client.println("500 Syntax error");
  else if (strcmp(parameters, _FTP_PASS.c_str()))
    client.println("530 ");
  else {
    if (debugMode) {
      Serial.println("I:OK. Waiting for commands.");
    }
    client.println("230 OK.");
    return true;
  }
  millisDelay = millis() + 100; // delay of 100 ms
  return false;
}

boolean FtpServer::processCommand()
{
  ///////////////////////////////////////
  //                                   //
  //      ACCESS CONTROL COMMANDS      //
  //                                   //
  ///////////////////////////////////////

  //
  //  CDUP - Change to Parent Directory
  //
  if (!strcmp(command, "CDUP") || (!strcmp(command, "CWD") && !strcmp(parameters, ".."))) {
    bool ok = false;
    if (strlen(cwdName) > 1) {
      // do nothing if cwdName is root
      // if cwdName ends with '/', remove it (must not append)
      if (cwdName[strlen(cwdName) - 1] == '/')
        cwdName[strlen(cwdName) - 1] = 0;
      // search last '/'
      char *pSep = strrchr(cwdName, '/');
      ok = pSep > cwdName;
      // if found, ends the string on its position
      if (ok) {
        *pSep = 0;
        ok = SD.exists(cwdName);
      }
    }
    // if an error appends, move to root
    if (!ok)
      strcpy(cwdName, "/");
    sprintf(ftpbuffer, "250 Ok. Current directory is %s", cwdName);
    client.println(ftpbuffer);
  }
  //
  //  CWD - Change Working Directory
  //
  else if (!strcmp(command, "CWD")) {
    char path[FTP_CWD_SIZE];
    if (haveParameter() && makeExistsPath(path)) {
      strcpy(cwdName, path);
      sprintf(ftpbuffer, "250 Ok. Current directory is %s", cwdName);
      client.println(ftpbuffer);
    }
  }
  //
  //  PWD - Print Directory
  //
  else if (!strcmp(command, "PWD")) {
    sprintf(ftpbuffer, "257 \"%s\" is your current directory", cwdName);
    client.println(ftpbuffer);
  }
  //
  //  QUIT
  //
  else if (!strcmp(command, "QUIT")) {
    disconnectClient();
    return false;
  }

  ///////////////////////////////////////
  //                                   //
  //    TRANSFER PARAMETER COMMANDS    //
  //                                   //
  ///////////////////////////////////////

  //
  //  MODE - Transfer Mode
  //
  else if (!strcmp(command, "MODE")) {
    if (!strcmp(parameters, "S")) {
      client.println("200 S Ok");
    }
    // else if( ! strcmp( parameters, "B" ))
    //  client.println( "200 B Ok\r\n";
    else {
      client.println("504 Only S(tream) is suported");
    }
  }
  //
  //  PASV - Passive Connection management
  //
  else if (!strcmp(command, "PASV")) {
    if (data.connected()) {
      data.stop();
    }
    // dataServer.begin();
    // dataIp = Ethernet.localIP();
    dataIp = WiFi.localIP();
    dataPort = FTP_DATA_PORT_PASV;
// data.connect( dataIp, dataPort );
// data = dataServer.available();
    if (debugMode) {
      Serial.println("I:Connection management set to passive");
      sprintf(ftpbuffer, "I:Data port set to %d", dataPort);
      Serial.println(ftpbuffer);
    }
    sprintf(ftpbuffer, "227 Entering Passive Mode (%d,%d,%d,%d,%d,%d).", dataIp[0], dataIp[1], dataIp[2], dataIp[3], dataPort >> 8, dataPort & 255);
    client.println(ftpbuffer);
    dataPassiveConn = true;
  }
  //
  //  PORT - Data Port
  //
  else if (!strcmp(command, "PORT")) {
    if (data) {
      data.stop();
    }
    // get IP of data client
    dataIp[0] = atoi(parameters);
    char *p = strchr(parameters, ',');
    for (uint8_t i = 1; i < 4; i++) {
      dataIp[i] = atoi(++p);
      p = strchr(p, ',');
    }
    // get port of data client
    dataPort = 256 * atoi(++p);
    p = strchr(p, ',');
    dataPort += atoi(++p);
    if (p == NULL) {
      client.println("501 Can't interpret parameters");
    } else {
      client.println("200 PORT command successful");
      dataPassiveConn = false;
    }
  }
  //
  //  STRU - File Structure
  //
  else if (!strcmp(command, "STRU")) {
    if (!strcmp(parameters, "F")) {
      client.println("200 F Ok");
    // else if( ! strcmp( parameters, "R" ))
    //  client.println( "200 B Ok\r\n";
    } else {
      client.println("504 Only F(ile) is supported");
    }
  }
  //
  //  TYPE - Data Type
  //
  else if (!strcmp(command, "TYPE")) {
    if (!strcmp(parameters, "A"))
      client.println("200 TYPE is now ASII");
    else if (!strcmp(parameters, "I"))
      client.println("200 TYPE is now 8-bit binary");
    else
      client.println("504 Unknown TYPE");
  }

  ///////////////////////////////////////
  //                                   //
  //        FTP SERVICE COMMANDS       //
  //                                   //
  ///////////////////////////////////////

  //
  //  ABOR - Abort
  //
  else if (!strcmp(command, "ABOR")) {
    abortTransfer();
    client.println("226 Data connection closed");
  }
  //
  //  DELE - Delete a File
  //
  else if (!strcmp(command, "DELE")) {
    char path[FTP_CWD_SIZE];

    if (strlen(parameters) == 0) {
      client.println("501 No file name");
    } else if (makePath(path)) {
      if (!SD.exists(path)) {
        sprintf(ftpbuffer, "550 File %s not found", parameters);
        client.println(ftpbuffer);
      } else {
        if (SD.remove(path)) {
          sprintf(ftpbuffer, "250 Deleted %s", parameters);
          client.println(ftpbuffer);
        } else {
          sprintf(ftpbuffer, "450 Can't delete %s", parameters);
          client.println(ftpbuffer);
        }
      }
    }
  }
  //
  //  LIST - List
  //
  else if (!strcmp(command, "LIST")) {
    if (debugMode) {
      Serial.println("I:LIST");
    }
    if (dataConnect()) {
      client.println("150 Accepted data connection");
      uint16_t nm = 0;
      File dir = SD.open(cwdName);
      if (!dir || !dir.isDirectory()) {
        sprintf(ftpbuffer, "550 Can't open directory %s", cwdName);
        client.println(ftpbuffer);
      } else {
        File file = dir.openNextFile();
        while (file) {
          String fn;
          fn = file.name();
          int i = fn.lastIndexOf("/") + 1;
          fn.remove(0, i);
          if (debugMode) {
            Serial.println("I:File Name = " + fn);
          }
          if (file.isDirectory()) {
            data.println("01-01-2000  00:00AM <DIR> " + fn);
          } else {
            sprintf(ftpbuffer, "01-01-2000  00:00AM %d %s", file.size(), fn.c_str());
            data.println(ftpbuffer);
            // data.println( " " + fn );
          }
          nm++;
          file = dir.openNextFile();
        }
        sprintf(ftpbuffer, "226 %d matches total", nm);
        client.println(ftpbuffer);
        data.stop();
      }
    } else {
      client.println("425 No data connection");
      data.stop();
    }
  }
  //
  //  MLSD - Listing for Machine Processing (see RFC 3659)
  //
  else if (!strcmp(command, "MLSD")) {
    if (debugMode) {
      Serial.println("I:MLSD");
    }
    if (!dataConnect()) {
      client.println("425 No data connection MLSD");
    } else {
      client.println("150 Accepted data connection");
      uint16_t nm = 0;
      // Dir dir= SD.openDir(cwdName);
      File dir = SD.open(cwdName);
      // if(!SD.exists(cwdName))
      if (!dir || !dir.isDirectory()) {
        sprintf(ftpbuffer, "550 Can't open directory %s", cwdName);
        client.println(ftpbuffer);
      } else {
        // while( dir.next())
        File file = dir.openNextFile();
        // while( dir.openNextFile())
        while (file) {
          String fn;
          fn = file.name();
          int pos = fn.lastIndexOf("/");
          fn.remove(0, pos + 1);
          if (file.isDirectory()) {
            data.println(fn);
            // data.println( "Type=dir;Size=" + fs + ";"+"modify=20000101000000;" +" " + fn);
            // data.println( "Type=dir;modify=20000101000000; " + fn);
          } else {
            sprintf(ftpbuffer, "%d %s", file.size(), fn.c_str());
            data.println(ftpbuffer);
            // data.println( "Type=file;Size=" + fs + ";"+"modify=20000101160656;" +" " + fn);
            // data.println( "Type=file;Size=" + fs + ";"+"modify=20000101000000;" +" " + fn);
          }
          nm++;
          file = dir.openNextFile();
        }
        client.println("226-options: -a -l");
        sprintf(ftpbuffer, "226 %d matches total", nm);
        client.println(ftpbuffer);
      }
      data.stop();
    }
  }
  //
  //  NLST - Name List
  //
  else if (!strcmp(command, "NLST")) {
    if (debugMode) {
      Serial.println("I:NLST");
    }
    if (!dataConnect()) {
      client.println("425 No data connection");
    } else {
      client.println("150 Accepted data connection");
      uint16_t nm = 0;
      // Dir dir=SD.openDir(cwdName);
      File dir = SD.open(cwdName);
      if (!SD.exists(cwdName)) {
        sprintf(ftpbuffer, "550 Can't open directory %s", parameters);
        client.println(ftpbuffer);
      } else {
        File file = dir.openNextFile();
        // while( dir.next())
        while (file) {
          // data.println( dir.fileName());
          data.println(file.name());
          nm++;
          file = dir.openNextFile();
        }
        sprintf(ftpbuffer, "226 %d matches total", nm);
        client.println(ftpbuffer);
      }
      data.stop();
    }
  }
  //
  //  NOOP
  //
  else if (!strcmp(command, "NOOP")) {
    // dataPort = 0;
    client.println("200 Zzz...");
  }
  //
  //  RETR - Retrieve
  //
  else if (!strcmp(command, "RETR")) {
    char path[FTP_CWD_SIZE];
    if (strlen(parameters) == 0) {
      client.println("501 No file name");
    } else if (makePath(path)) {
      file = SD.open(path, "r");
      if (!file) {
        sprintf(ftpbuffer, "550 File %s not found", parameters);
        client.println(ftpbuffer);
      } else if (!file) {
        sprintf(ftpbuffer, "450 Can't open %s", parameters);
        client.println(ftpbuffer);
      } else if (!dataConnect()) {
        client.println("425 No data connection");
      } else {
        if (debugMode) {
          Serial.println("I:Sending " + String(parameters));
        }
        sprintf(ftpbuffer, "150-Connected to port %d", dataPort);
        client.println(ftpbuffer);
        sprintf(ftpbuffer, "150 %d bytes to download", file.size());
        client.println(ftpbuffer);
        millisBeginTrans = millis();
        bytesTransfered = 0;
        transferStatus = 1;
      }
    }
  }
  //
  //  STOR - Store
  //
  else if (!strcmp(command, "STOR")) {
    char path[FTP_CWD_SIZE];

    if (debugMode) {
      Serial.println("I:STOR");
    }
    if (strlen(parameters) == 0) {
      client.println("501 No file name");
    } else if (makePath(path)) {
      file = SD.open(path, "w");
      if (!file) {
        sprintf(ftpbuffer, "451 Can't open/create %s", parameters);
        client.println(ftpbuffer);
      } else if (!dataConnect()) {
        client.println("425 No data connection");
        file.close();
      } else {
        if (debugMode) {
          Serial.println("I:Receiving " + String(parameters));
        }
        sprintf(ftpbuffer, "150 Connected to port %d", dataPort);
        client.println(ftpbuffer);
        millisBeginTrans = millis();
        bytesTransfered = 0;
        transferStatus = 2;
      }
    }
  }
  //
  //  MKD - Make Directory
  //
  else if (!strcmp(command, "MKD")) {
    if (debugMode) {
      Serial.print("MKD P=");
      Serial.print(parameters);
      Serial.print(" CWD=");
      Serial.println(cwdName);
    }
    if (!strcmp(cwdName, "/")) { // avoid "\\newdir"
      sprintf(ftpbuffer, "/%s", parameters);
    } else {
      sprintf(ftpbuffer, "%s/%s", cwdName, parameters);
    }

    if (debugMode) {
      Serial.print("try to create  ");
      Serial.println(ftpbuffer);
    }

    fs::FS &fs = SD;
    if (fs.mkdir(ftpbuffer)) {
      sprintf(ftpbuffer, "257 \"%s\" - Directory successfully created", parameters);
      client.println(ftpbuffer);
    } else {
      sprintf(ftpbuffer, "502 Can't create \"%s\"", parameters);
      client.println(ftpbuffer);
    }
  }
  //
  //  RMD - Remove a Directory
  //
  else if (!strcmp(command, "RMD")) {
    if (debugMode) {
      Serial.print("RMD ");
      Serial.print(parameters);
      Serial.print(" CWD=");
      Serial.println(cwdName);
    }
    if (!strcmp(cwdName, "/")) { // avoid "\\newdir"
      sprintf(ftpbuffer, "/%s", parameters);
    } else {
      sprintf(ftpbuffer, "%s/%s", cwdName, parameters);
    }
    fs::FS &fs = SD;
    if (fs.rmdir(ftpbuffer)) {
      client.println("250 RMD command successful");
    } else {
      sprintf(ftpbuffer, "502 Can't delete \"%s\"", parameters);
      client.println(ftpbuffer); // not support on espyet
    }
  }

  ///////////////////////////////////////
  //                                   //
  //   EXTENSIONS COMMANDS (RFC 3659)  //
  //                                   //
  ///////////////////////////////////////

  //
  //  FEAT - New Features
  //
  else if (!strcmp(command, "FEAT")) {
    client.println("211-Extensions supported:");
    client.println(" MLSD");
    client.println("211 End.");
  }
  //
  //  SIZE - Size of the file
  //
  else if (!strcmp(command, "SIZE")) {
    char path[FTP_CWD_SIZE];
    if (strlen(parameters) == 0) {
      client.println("501 No file name");
    } else if (makePath(path)) {
      file = SD.open(path, "r");
      if (!file) {
        sprintf(ftpbuffer, "450 Can't open \"%s\"", parameters);
        client.println(ftpbuffer);
      } else {
        sprintf(ftpbuffer, "213 %d", file.size());
        client.println(ftpbuffer);
        file.close();
      }
    }
  }
  //
  //  SITE - System command
  //
  else if (!strcmp(command, "SITE")) {
    sprintf(ftpbuffer, "500 Unknown SITE command %s", parameters);
    client.println(ftpbuffer);
  }
  //
  //  Unrecognized commands ...
  //
  else
    client.println("500 Unknown command");
  return true;
}

boolean FtpServer::dataConnect()
{
  unsigned long startTime = millis();
  // wait 5 seconds for a data connection
  if (!data.connected()) {
    while (!dataServer.hasClient() && millis() - startTime < 10000) {
      // while (!dataServer.available() && millis() - startTime < 10000)
      // delay(100);
      yield();
    }
    if (dataServer.hasClient()) {
      // if (dataServer.available()) {
      data.stop();
      data = dataServer.available();
      if (debugMode) {
        Serial.println("I:ftpdataserver client....");
      }
    }
  }
  return data.connected();
}

boolean FtpServer::doRetrieve()
{
  if (data.connected()) {
    int16_t nb = file.readBytes(buf, FTP_BUF_SIZE);

    if (nb > 0) {
      size_t cnt = 0;

      while (cnt < nb) {
        cnt += data.write((uint8_t *)&buf[cnt], nb);
        yield();
        delay(1);
        if (cnt == 0) {
          Serial.println("I:ABORT");
          closeTransfer();
          return false;
        }
        bytesTransfered += cnt;
        nb -= cnt;
        if (nb > 0) {
          sprintf(ftpbuffer, "I:%d", nb);
          Serial.println(ftpbuffer);
          yield();
          delay(10);
        }
      }
      return true;
    }
  }
  closeTransfer();
  yield();
  delay(10);
  return false;
}

boolean FtpServer::doStore()
{
  // Serial.println("doStore:data.connected " + String(data.connected()));
  if (data.connected()) {
    int16_t nb = data.readBytes((uint8_t *)buf, FTP_BUF_SIZE);
    // Serial.println("doStore:nb " + String(nb) + " " + String(bytesTransfered));
    if (nb > 0) {
      // Serial.println( millis() << " " << nb << endl;
      file.write((uint8_t *)buf, nb);
      bytesTransfered += nb;
    }
    return true;
  }
  closeTransfer();
  return false;
}

void FtpServer::closeTransfer()
{
  // Serial.println("closeTransfer");
  uint32_t deltaT = (int32_t)(millis() - millisBeginTrans);
  if (deltaT > 0 && bytesTransfered > 0) {
    client.println("226-File successfully transferred");
    sprintf(ftpbuffer, "226 %d ms, %d kbytes/s", deltaT, bytesTransfered / deltaT);
    client.println(ftpbuffer);
  } else {
    client.println("226 File successfully transferred");
  }
  file.close();
  data.stop();
}

void FtpServer::abortTransfer()
{
  if (transferStatus > 0) {
    file.close();
    data.stop();
    client.println("426 Transfer aborted");
    if (debugMode) {
      Serial.println("W:Transfer aborted!");
    }
  }
  transferStatus = 0;
}

// Read a char from client connected to ftp server
//
//  update cmdLine and command buffers, iCL and parameters pointers
//
//  return:
//    -2 if ftpbuffer cmdLine is full
//    -1 if line not completed
//     0 if empty line received
//    length of cmdLine (positive) if no empty line received

int8_t FtpServer::readChar()
{
  int8_t rc = -1;

  if (client.available()) {
    char c = client.read();
// char c;
// client.readBytes((uint8_t*) c, 1);
    if (debugMode) {
      Serial.print(c);
    }
    if (c == '\\') {
      c = '/';
    }
    if (c != '\r') {
      if (c != '\n') {
        if (iCL < FTP_CMD_SIZE) {
          cmdLine[iCL++] = c;
        } else {
          rc = -2; // Line too long
        }
      } else {
        cmdLine[iCL] = 0;
        command[0] = 0;
        parameters = NULL;
        // empty line?
        if (iCL == 0) {
          rc = 0;
        } else {
          rc = iCL;
          // search for space between command and parameters
          parameters = strchr(cmdLine, ' ');
          if (parameters != NULL) {
            if (parameters - cmdLine > 4) {
              rc = -2; // Syntax error
            } else {
              strncpy(command, cmdLine, parameters - cmdLine);
              command[parameters - cmdLine] = 0;
              while (*(++parameters) == ' ');
            }
          } else if (strlen(cmdLine) > 4) {
            rc = -2; // Syntax error.
          } else {
            strcpy(command, cmdLine);
          }
          iCL = 0;
        }
      }
    }
    if (rc > 0) {
      for (uint8_t i = 0; i < strlen(command); i++) {
        command[i] = toupper(command[i]);
      }
    }
    if (rc == -2) {
      iCL = 0;
      client.println("500 Syntax error");
    }
  }
  return rc;
}

// Make complete path/name from cwdName and parameters
//
// 3 possible cases: parameters can be absolute path, relative path or only the name
//
// parameters:
//   fullName : where to store the path/name
//
// return:
//    true, if done

boolean FtpServer::makePath(char *fullName)
{
  return makePath(fullName, parameters);
}

boolean FtpServer::makePath(char *fullName, char *param)
{
  if (param == NULL)
    param = parameters;
  // Root or empty?
  if (strcmp(param, "/") == 0 || strlen(param) == 0)
  {
    strcpy(fullName, "/");
    return true;
  }
  // If relative path, concatenate with current dir
  if (param[0] != '/')
  {
    strcpy(fullName, cwdName);
    if (fullName[strlen(fullName) - 1] != '/')
      strncat(fullName, "/", FTP_CWD_SIZE);
    strncat(fullName, param, FTP_CWD_SIZE);
  } else {
    strcpy(fullName, param);
  }
  // If ends with '/', remove it
  uint16_t strl = strlen(fullName) - 1;
  if (fullName[strl] == '/' && strl > 1)
    fullName[strl] = 0;
  if (strlen(fullName) < FTP_CWD_SIZE)
    return true;
  client.println("500 Command line too long");
  return false;
}

// Calculate year, month, day, hour, minute and second
//   from first parameter sent by MDTM command (YYYYMMDDHHMMSS)
//
// parameters:
//   pyear, pmonth, pday, phour, pminute and psecond: pointer of
//     variables where to store data
//
// return:
//    0 if parameter is not YYYYMMDDHHMMSS
//    length of parameter + space

uint8_t FtpServer::getDateTime(uint16_t *pyear, uint8_t *pmonth, uint8_t *pday,
                               uint8_t *phour, uint8_t *pminute, uint8_t *psecond)
{
  char dt[15];
  // Date/time are expressed as a 14 digits long string
  // terminated by a space and followed by name of file
  if (strlen(parameters) < 15 || parameters[14] != ' ')
    return 0;
  for (uint8_t i = 0; i < 14; i++)
    if (!isdigit(parameters[i]))
      return 0;
  strncpy(dt, parameters, 14);
  dt[14] = 0;
  *psecond = atoi(dt + 12);
  dt[12] = 0;
  *pminute = atoi(dt + 10);
  dt[10] = 0;
  *phour = atoi(dt + 8);
  dt[8] = 0;
  *pday = atoi(dt + 6);
  dt[6] = 0;
  *pmonth = atoi(dt + 4);
  dt[4] = 0;
  *pyear = atoi(dt);
  return 15;
}

// Create string YYYYMMDDHHMMSS from date and time
//
// parameters:
//    date, time
//    tstr: where to store the string. Must be at least 15 characters long
//
// return:
//    pointer to tstr

char *FtpServer::makeDateTimeStr(char *tstr, uint16_t date, uint16_t time)
{
  sprintf(tstr, "%04u%02u%02u%02u%02u%02u",
          ((date & 0xFE00) >> 9) + 1980, (date & 0x01E0) >> 5, date & 0x001F,
          (time & 0xF800) >> 11, (time & 0x07E0) >> 5, (time & 0x001F) << 1);
  return tstr;
}

bool FtpServer::haveParameter()
{
  if (parameters != NULL && strlen(parameters) > 0)
    return true;
  client.println("501 No file name");
  return false;
}

bool FtpServer::makeExistsPath(char *path, char *param)
{
  if (!makePath(path, param))
    return false;
  if (SD.exists(path))
    return true;
  sprintf(ftpbuffer, "550 %s not found.", path);
  client.println(ftpbuffer);
  return false;
}
