#ifndef _MHZ19_H
#define _MHZ19_H

#include <MHZ19.h>

#include "common.h"

// Interface is UART

// CO2 Sensor Pins
#define CO2_IN_PIN    27
#define MH_Z19_RX_PIN 16
#define MH_Z19_TX_PIN 17

char mhz19_version[5] = { 0, 0, 0, 0, 0 };

MHZ19 mhz19;
HardwareSerial mhz19Serial(2);

bool setupMHZ19(int16_t y) {
  // Init MHZ19 - CO2 Sensor  
  mhz19Serial.begin(9600);
  mhz19.begin(mhz19Serial);                                // *Serial(Stream) reference must be passed to library begin().
  mhz19.autoCalibration();                              // Turn auto calibration ON (OFF autoCalibration(false))
  mhz19.getVersion(mhz19_version);
  //Clear array from maybe random entries
  for (int i = 0; i < HISTORY_SIZE; i++)
    histCO2[i] = 0;
  return true;
}

void updateMHZ19(mhz19b_t *p) {
  p->co2 = mhz19.getCO2();
  p->temp = mhz19.getTemperature();
  p->error = mhz19.errorCode;

  temp.mhz19 = mhz19.getTemperature();
  p->temp = lround(temp.mhz19 * 100.0); // °C
  temp.mhz19 +=  offsets.mhz19_temp;
}

#endif // _MHZ19_H
