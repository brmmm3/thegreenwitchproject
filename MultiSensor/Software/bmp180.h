#ifndef _BMP180_H
#define _BMP180_H

#include <BMP085.h>

#include "float16.h"
#include "common.h"

// Interface is I2C address 0x77

BMP085 bmp180;

bool setupBMP180(int16_t y) {
  bmp180.init();
  return true;
}

void resetBMP180() {
  bmp180.init();
}

void updateBMP180(bmp180_t *p) {
  temp.bmp180 = bmp180.bmp085GetTemperature(bmp180.bmp085ReadUT());
  p->temp = lround(temp.bmp180 * 100.0); // °C
  temp.bmp180 +=  offsets.bmp180_temp;
}

void updateBMP180Pressure(bmp180_t *p) {
  if (p->cnt >= PRESSURE_SIZE) {
    return;
  }
  pressure.bmp180 = bmp180.bmp085GetPressure(bmp180.bmp085ReadUP()) / 100.0;
  p->pressure[p->cnt++] = f32tof16(pressure.bmp180); // hPa
  pressure.bmp180 += offsets.bmp180_pressure;
}
 
#endif // _BMP180_H
