#ifndef _DISPLAY_H
#define _DISPLAY_H

#include <sd_defines.h>
#include <sd_diskio.h>
#include <SD.h>
#include <TFT_eSPI.h>

#include "common.h"
#include "bitmap.h"   //This is a file in the same dir from this program(!!!)
#include "button.h"
#include "ftpserver.h"

// Pins
#define TFT_MISO_PIN 19
#define TFT_MOSI_PIN 23
#define TFT_SCLK_PIN 18
#define TFT_CS_PIN   14
#define TFT_DC_PIN   25
#define TFT_BL_PIN   26

#define TFT TFT_eSPI

TFT_eSPI tft = TFT_eSPI(300, 240); // Init ST7789 280x240. 300 is a needed workaround for TFT_eSPI.

#define ICO_GPS_X     52
#define ICO_WEB_X     72
#define ICO_SDCARD_X  92

uint8_t oldDataCnt = 255;
uint8_t oldSecond = 0;

uint8_t oldGpsAvailable = 0;
bool oldWiFiEnabled = false;
bool oldWiFiScanning = false;
int8_t oldWiFiRSSI = 0;
bool oldRecordingEnabled = false;
bool tmpRecordingEnabled = false;
int8_t oldFtpTransferStatus = 0;


void setupDisplay() {
  tft.init();
  tft.fillScreen(TFT_BLACK); // Clear screen
  tft.setRotation(2);        // Set rotation to landscape
  tft.setViewport(20, 0, 280, 240); // Another needed workaround. We have to move the viewport.
  pinMode(TFT_BL_PIN, OUTPUT);
  digitalWrite(TFT_BL_PIN, BacklightEnabled);
}

void setBacklight(bool enabled) {
  BacklightEnabled = enabled;
  digitalWrite(TFT_BL_PIN, BacklightEnabled);
}

void clearMax() {
  displayInfo.co2_max = 0;
  displayInfo.o2_max = 0;
  displayInfo.no2_max = 0;
  displayInfo.c2h5oh_max = 0;
  displayInfo.voc_max = 0;
  displayInfo.co_max = 0;
  displayInfo.pm25_max = 0;
  displayInfo.pm10_max = 0;
}

void showInfoLine(int16_t y, char *title, char *value) {
  tft.setTextDatum(TR_DATUM);
  tft.drawString(value, 270, y);
  tft.setTextDatum(TL_DATUM);
  tft.drawString(title, 20, y);
}

void drawText(int16_t y, char* title, char* unit, char* value, uint16_t color = TFT_WHITE, int32_t dx = 0,
              bool serial = true, bool force_update = false) {
  if (SerialOut > 1 && serial) {
    sprintf(tmpbuffer, "I: %s: %s %s", title, value, unit);
    Serial.println(tmpbuffer);
  }
  if (force_update || (counter == 0)) {
    tft.fillRect(20, y, 260, 18, TFT_BLACK);
    tft.setTextColor(TFT_WHITE);
    tft.setCursor(20, y);
    tft.print(title);
    tft.print(": ");
  }
  tft.setTextColor(color);
  tft.fillRect(100, y, 85 + dx, 18, TFT_BLACK);
  tft.setTextDatum(TR_DATUM);
  tft.drawString(value, 180 + dx, y);
  tft.setTextDatum(TL_DATUM);
  if (force_update || (counter == 0)) {
    tft.setTextColor(TFT_WHITE);
    tft.drawString(unit, 185 + dx, y);
  }
}

void drawText(int16_t y, char* title, char* unit, float value, uint16_t color = TFT_WHITE, int32_t dx = 0,
              bool serial = true, bool force_update = false) {
  sprintf(buffer, "%.1f", value);
  drawText(y, title, unit, buffer, color, dx, serial, force_update);
}

void drawText(int16_t y, char* title, char* unit, float value, bool force_update = false) {
  sprintf(buffer, "%.1f", value);
  drawText(y, title, unit, buffer, TFT_WHITE, 0, true, force_update);
}

void drawMaxText(int16_t y, char *title, char *unit, float value, float max, const float* limits, unsigned int decimals, bool force_update = false) {
  uint16_t color = TFT_WHITE;
  if (value < limits[0] || value > limits[3]) {
    color = TFT_ORANGE;
  } else if (value < limits[1] || value > limits[2]) {
    color = TFT_YELLOW;
  }
  if (SerialOut > 1) {
    sprintf(buffer, "I:%s: %.1f %s (Max=%.1f)", title, value, unit, max);
    Serial.println(buffer);
  }
  tft.setTextSize(2);
  tft.setTextDatum(TL_DATUM);
  if (decimals > 0) {
    drawText(y, title, unit, value, color, 0, false, force_update);
  } else {
    sprintf(buffer, "%ld", lround(value));
    drawText(y, title, unit, buffer, color, 0, false, force_update);
  }
  // Draw max
  int16_t w = 28;

  if (max >= 100.0) {
    w = 37;
  } else if (max >= 1000.0) {
    w = 42;
  }
  tft.setTextDatum(TR_DATUM);
  tft.setTextSize(1);
  tft.setTextColor(color);
  tft.fillRect(272 - w, y, w, 8, TFT_BLACK);
  if (decimals > 0) {
    tft.drawFloat(max, decimals, 272, y, 1);
  } else {
    tft.drawNumber(lround(max), 272, y, 1);
  }
  tft.setTextSize(2);
  tft.setTextDatum(TL_DATUM);
}

void drawMinMaxText(int16_t y, char *title, char *unit, float value, float min, float max, unsigned int decimals, bool force_update = false) {
  float delta = (max - min) / value;
  uint16_t color = TFT_WHITE;
  if (delta > 0.25) {
    color = TFT_ORANGE;
  } else if (delta > 0.1) {
    color = TFT_YELLOW;
  }
  if (SerialOut > 1) {
    sprintf(buffer, "I:%s: %.1f %s (Min=%.1f, Max=%.1f)", title, value, unit, min, max);
    Serial.println(buffer);
  }
  tft.setTextSize(2);
  tft.setTextDatum(TL_DATUM);
  if (decimals > 0) {
    drawText(y, title, unit, value, color, 0, false, force_update);
  } else {
    sprintf(buffer, "%ld", lround(value));
    drawText(y, title, unit, buffer, color, 0, false, force_update);
  }
  // Draw min/max
  tft.setTextDatum(TR_DATUM);
  tft.setTextSize(1);
  tft.setTextColor(color);
  tft.fillRect(236, y, 36, 18, TFT_BLACK);
  if (decimals > 0) {
    tft.drawFloat(min, decimals, 272, y, 1);
    tft.drawFloat(max, decimals, 272, y + 9, 1);
  } else {
    tft.drawNumber(lround(min), 272, y, 1);
    tft.drawNumber(lround(max), 272, y + 9, 1);
  }
  tft.setTextSize(2);
  tft.setTextDatum(TL_DATUM);
}

void clearScreen() {
  tft.fillRect(8, 20, 272, 220, TFT_BLACK);
}

void hideSDIcon() {
  tft.fillRect(ICO_SDCARD_X, 0, 16, 16, TFT_BLACK);
}

void hideWiFiIcon() {
  tft.fillRect(ICO_WEB_X, 0, 16, 16, TFT_BLACK);
}

void updateStatusBar(bool force) {
  if (force || oldGpsAvailable != GpsAvailable) {
    oldGpsAvailable = GpsAvailable;
    if (GpsAvailable >= 10) {
      if (GpsAvailable > 13) {
        tft.drawBitmap(ICO_GPS_X, 0, (uint8_t *)ico_gps, 16, 16, TFT_GREEN);
      } else if (GpsAvailable > 10) {
        tft.drawBitmap(ICO_GPS_X, 0, (uint8_t *)ico_gps, 16, 16, TFT_YELLOW);
      } else {
        tft.drawBitmap(ICO_GPS_X, 0, (uint8_t *)ico_gps, 16, 16, TFT_WHITE);
      }
    } else {
      tft.drawBitmap(ICO_GPS_X, 0, (uint8_t *)ico_gps, 16, 16, TFT_RED);
    }
  }
  // WiFi
  if (force || oldWiFiEnabled != WiFiEnabled || oldWiFiScanning != WiFiScanning || oldWiFiRSSI != WiFiRSSI) {
    oldWiFiEnabled = WiFiEnabled;
    oldWiFiScanning = WiFiScanning;
    oldWiFiRSSI = WiFiRSSI;
    tft.fillRect(ICO_WEB_X, 0, 16, 16, TFT_BLACK);
    if (WiFiEnabled) {
      if (WiFiScanning) {
        tft.drawBitmap(ICO_WEB_X, 0, (uint8_t *)ico_wifi_very_good, 16, 16, TFT_YELLOW);
      } else {
        if (WiFiRSSI >= -30) {
          wifi_icon = (uint8_t *)ico_wifi_very_good;
        } else if (WiFiRSSI >= -67) {
          wifi_icon = (uint8_t *)ico_wifi_good;    
        } else if (WiFiRSSI >= -70) {
          wifi_icon = (uint8_t *)ico_wifi_medium;
        } else {
          wifi_icon = (uint8_t *)ico_wifi_bad;
        }
        tft.drawBitmap(ICO_WEB_X, 0, wifi_icon, 16, 16, TFT_WHITE);
      }
    } else {
      tft.drawBitmap(ICO_WEB_X, 0, (uint8_t *)ico_wifi_very_good, 16, 16, TFT_DARKGREEN);
    }
  }
  // SD-Card
  if (force || oldRecordingEnabled != RecordingEnabled || oldSdCardType != sdInfo.card_type) {
    oldRecordingEnabled = RecordingEnabled;
    oldSdCardType = sdInfo.card_type;
    if (sdInfo.card_type != CARD_NONE) {
      if (RecordingEnabled) {
        tft.drawBitmap(ICO_SDCARD_X, 0, (uint8_t *)ico_disk, 16, 16, TFT_WHITE);
      } else {
        tft.drawBitmap(ICO_SDCARD_X, 0, (uint8_t *)ico_disk, 16, 16, TFT_DARKGREEN);
      }
    } else {
      tft.drawBitmap(ICO_SDCARD_X, 0, (uint8_t *)ico_disk, 16, 16, TFT_RED);
    }
  }
  if (clockInfo.second == oldSecond) {
    return;
  }
  oldSecond = clockInfo.second;
  tft.fillRect(136, 0, 144, 20, TFT_BLACK);
  tft.setCursor(136, 0);
  tft.setTextSize(2);
  tft.setTextColor(TFT_WHITE);
  tft.print(clockInfoToTimeString());
}

void updateProgressBar(uint8_t data_cnt) {
  if (data_cnt >= DATA_SIZE) {
    tft.fillRect(0, 20, 8, 200, TFT_GREEN);
  } else {
    int h = data_cnt * 200 / DATA_SIZE;
    tft.fillRect(0, 220 - h, 8, h, TFT_WHITE);
  }
}

void showSensorData(bool force) {
  // Update Sensor data
  drawMinMaxText(20, (char *)"Temp", (char *)"C", displayInfo.temp, displayInfo.temp_min, displayInfo.temp_max, 1, force);
  drawMinMaxText(38, (char *)"Humi", (char *)"%", displayInfo.humidity, displayInfo.humidity_min, displayInfo.humidity_max, 1, force);
  drawMinMaxText(56, (char *)"Pres", (char *)"hPa", displayInfo.pressure, displayInfo.pressure_min, displayInfo.pressure_max, 0, force);
  sprintf(buffer, "%ld", lround(calcAltitude(displayInfo.pressure)));
  drawText(74, (char *)"Alt", (char *)"m", buffer, TFT_WHITE, 0, false, force);
  drawMaxText(92, (char *)"CO2", (char *)"ppm", displayInfo.co2, displayInfo.co2_max, co2_limits, 1, force);
  drawMaxText(110, (char *)"O2", (char *)"%", displayInfo.o2, displayInfo.o2_max, co_limits, 1, force);
  drawMaxText(128, (char *)"NO2", (char *)"ppm", displayInfo.no2, displayInfo.no2_max, no2_limits, 1, force);
  drawMaxText(146, (char *)"C2H5OH", (char *)"ppm", displayInfo.c2h5oh, displayInfo.c2h5oh_max, c2h5oh_limits, 1, force);
  drawMaxText(164, (char *)"VOC", (char *)"ppm", displayInfo.voc, displayInfo.voc_max, voc_limits, 1, force);
  drawMaxText(182, (char *)"CO", (char *)"ppm", displayInfo.co, displayInfo.co_max, co_limits, 1, force);
  // PM2.5, PM10
  if (displayInfo.pm_valid) {
    drawMaxText(200, (char *)"PM2.5", (char *)"ug/m3", displayInfo.pm25, displayInfo.pm25_max, pm25_limits, 1, true);
    drawMaxText(218, (char *)"PM10", (char *)"ug/m3", displayInfo.pm10, displayInfo.pm10_max, pm10_limits, 1, true);
  } else {
    tft.fillRect(20, 200, 260, 40, TFT_BLACK);
    sprintf(buffer, "%s", SDS011Error.c_str());
    drawText(200, (char *)"PM", (char *)"", buffer, TFT_ORANGE, 56, false, true);
  }
}

void showPositionData(bool force) {
  sprintf(buffer, "%d", displayInfo.altitude);
  drawText(20, (char *)"Alt", (char *)"m", buffer, TFT_WHITE, 0, false, force);
  sprintf(buffer, "%.1f", displayInfo.atm);
  drawText(40, (char *)"ATM", (char *)"%", buffer, TFT_WHITE, 0, false, force);
  sprintf(buffer, "%d", displayInfo.pa1010d_sat);
  drawText(60, (char *)"Sat1", (char *)"", buffer, TFT_WHITE, 0, false, force);
  drawText(80, (char *)"Alt1", (char *)"m", displayInfo.pa1010d_alt, force);
  drawText(100, (char *)"Lng1", (char *)"DEG", displayInfo.pa1010d_lng, force);
  drawText(120, (char *)"Lat1", (char *)"DEG", displayInfo.pa1010d_lat, force);
}

void showHistoryChart(String title, int16_t *data, float div) {
  float y_old;
  float y_max = 0.0;
  float y_fact = 1.0;
  uint8_t i;

  for (i = 0; i < HISTORY_SIZE; i++) {
    if (data[i] > y_max) y_max = (float)data[i];
  }
  if (y_max > 0.0) {
    y_fact = 198.0 / y_max;
  }
  clearScreen();
  tft.drawString(title, 20, 20);
  tft.drawRect(49, 40, HISTORY_SIZE + 2, 200, TFT_DARKGREEN);
  y_old = (float)data[0] * y_fact;
  for (i = 1; i < HISTORY_SIZE; i++) {
    float y = (float)data[i] * y_fact;

    tft.drawLine(50 + i, 239 - y_old, 51 + i, 239 - y, TFT_WHITE);
    y_old = y;
  }
  tft.setTextDatum(TR_DATUM);
  sprintf(buffer, "%.1f", y_max / div);
  tft.drawString(buffer, 264, 20);
  tft.setTextDatum(TL_DATUM);
}

void showDeviceInfo() {
  showInfoLine(20, (char *)"SD Card Type", getSdCardType());
  sprintf(buffer, "%.1f GB", (float)sdInfo.card_size / GB);
  showInfoLine(40, (char *)"Card Size", buffer);
  sprintf(buffer, "%.1f GB", (float)sdInfo.total_bytes / GB);
  showInfoLine(60, (char *)"Volume Size", buffer);
  sprintf(buffer, "%.1f MB", (float)sdInfo.used_bytes / MB);
  showInfoLine(80, (char *)"Used Size", buffer);
  sprintf(buffer, "%d", sdInfo.file_cnt);
  showInfoLine(100, (char *)"File Cnt", buffer);
  sprintf(buffer, "%d", startup_cnt);
  showInfoLine(120, (char *)"Start Cnt", buffer);
  sprintf(buffer, "%.1f h", (float)operating_time / 60.0);
  showInfoLine(140, (char *)"Oper. Time", buffer);
  tft.drawString(fileName, 20, 160);
}

void showMenu(bool selected) {
  clearScreen();
  tft.drawString("Exit Menu", 40, 20);
  if (DisplayAlwaysOn) {
    tft.drawString("Display OFF", 40, 40);
  } else {
    tft.drawString("Display ON", 40, 40);
  }
  if (WiFiEnabled) {
    tft.drawString("WiFi OFF", 40, 60);
  } else {
    tft.drawString("WiFi ON", 40, 60);
  }
  if (RecordingEnabled) {
    tft.drawString("Recording OFF", 40, 80);
  } else {
    tft.drawString("Recording ON", 40, 80);
  }
  if (SerialOut) {
    tft.drawString("Serial OFF", 40, 100);
  } else {
    tft.drawString("Serial ON", 40, 100);
  }
  tft.drawString("Reset Max", 40, 120);
  tft.drawString("Home Mode", 40, 140);
  if (MenuPos < 7) {
    if (selected) {
      tft.fillRect(28, 20 + MenuPos * 20, 8, 16, TFT_GREEN);
    } else {
      tft.fillRect(28, 20 + MenuPos * 20, 8, 16, TFT_WHITE);
    }
  }
}

void updateDisplay(bool force) {
  updateStatusBar(force);
  if (!force && displayInfo.data_cnt == oldDataCnt) {
    return;
  }
  oldDataCnt = displayInfo.data_cnt;
  updateProgressBar(displayInfo.data_cnt);
  tft.setTextSize(2);
  tft.setTextColor(TFT_WHITE);
  if (DisplayPage == SENSORS) {
    showSensorData(force);
  } else if (DisplayPage == POSITION) {
    showPositionData(force);
  } else if (DisplayPage == TEMPERATURE) {
    showHistoryChart("Temp [C]", histTemp, 10.0);
  } else if (DisplayPage == HUMIDITY) {
    showHistoryChart("Humidity [%]", histHumidity, 10.0);
  } else if (DisplayPage == PRESSURE) {
    showHistoryChart("Pressure [hPa]", histPressure, 1.0);
  } else if (DisplayPage == ALTITUDE) {
    showHistoryChart("Altitude [m]", histAltitude, 1.0);
  } else if (DisplayPage == CO2) {
    showHistoryChart("CO2 [ppm]", histCO2, 10.0);
  } else if (DisplayPage == O2) {
    showHistoryChart("O2 [%]", histO2, 10.0);
  } else if (DisplayPage == NO2) {
    showHistoryChart("NO2 [%]", histNO2, 10.0);
 } else if (DisplayPage == C2H5OH) {
    showHistoryChart("C2H5OH [%]", histC2H5OH, 10.0);
 } else if (DisplayPage == VOC) {
    showHistoryChart("VOC [%]", histVOC, 10.0);
 } else if (DisplayPage == CO) {
    showHistoryChart("CO [%]", histCO, 10.0);
  } else if (DisplayPage == PM25) {
    showHistoryChart("PM25 1um [ug/m3]", histPM25, 10.0);
  } else if (DisplayPage == PM10) {
    showHistoryChart("PM10 2.5um [ug/m3]", histPM10, 10.0);
  } else if (DisplayPage == DEV_INFO) {
    showDeviceInfo();
  }
}

void updateFtpStatus(bool force) {
    updateStatusBar(force);

    int8_t ftpTransferStatus = getFtpTransferStatus();

    if (oldFtpTransferStatus != ftpTransferStatus) {
      tft.fillRect(0, 140, 280, 150, TFT_BLACK);
      tft.setTextSize(2);
      tft.setTextColor(TFT_WHITE);
      tft.setCursor(55, 150);
      if (ftpTransferStatus == 1) {
        tft.print("Downloading...");
      } else if (ftpTransferStatus == 2) {
        tft.print("Uploading...");
      }
      oldFtpTransferStatus = ftpTransferStatus;
    }
    if (FtpConnected) return;
    Serial.println("I:FTP client connected.");
    FtpConnected = true;
    tmpRecordingEnabled = RecordingEnabled;
    RecordingEnabled = false;
    clearScreen();
    tft.setTextSize(3);
    tft.setTextColor(TFT_WHITE);
    tft.setCursor(45, 80);
    tft.print("FTP Client");
    tft.setCursor(55, 110);
    tft.print("Connected");
}

void showMessage(char* msg) {
  tft.fillRect(20, 220, 240, 20, TFT_BLACK);
  tft.drawString(msg, 20, 220);
  if (SerialOut > 1) {
    Serial.print("I:");
    Serial.println(msg);
  }
}

void updateUser(bool force) {
  if (MenuPos < 255) {
    if (UpdateMenu) {
      showMenu(false);
      UpdateMenu = false;
    }
    return;
  }
  if (isFtpClientConnected()) {
    return updateFtpStatus(force);
  }
  if (FtpConnected) {
    Serial.println("I:FTP client disconnected.");
    FtpConnected = false;
    RecordingEnabled = tmpRecordingEnabled;
    data_cnt = 0;
    counter = 0;
    displayInfo.data_cnt = 0;
    clearScreen();
  }
  if (oldDataCnt > displayInfo.data_cnt) {
    tft.fillRect(0, 20, 8, 200, TFT_BLACK);
  }
  updateDisplay(force);
  // Write data to SD-Card
  if (RecordingEnabled && (displayInfo.data_cnt >= DATA_SIZE)) {
    tft.drawBitmap(ICO_SDCARD_X, 0, (uint8_t *)ico_disk, 16, 16, TFT_YELLOW);
    
    // Write data to SD-Card
    if (writeDataToFile()) {
      tft.drawBitmap(ICO_SDCARD_X, 0, (uint8_t *)ico_disk, 16, 16, TFT_WHITE);
      sprintf(buffer, "Written %s", fileName);
      showMessage(buffer);
    } else {
      tft.drawBitmap(ICO_SDCARD_X, 0, (uint8_t *)ico_disk, 16, 16, TFT_RED);
    }
    tft.fillRect(0, 20, 8, 200, TFT_BLACK);
    displayInfo.data_cnt = 0;
    if (SerialOut) {
      sprintf(buffer, "I:MinFreeHeapSize=%d", esp_get_minimum_free_heap_size());
      Serial.println(buffer);
    }
  } else if (!RecordingEnabled) {
    tft.fillRect(0, 20, 8, 200, TFT_BLACK);
  }
}

#endif // _DISPLAY_H
