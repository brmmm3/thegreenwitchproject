#ifndef _SD_H
#define _SD_H

#include <sd_defines.h>
#include <sd_diskio.h>
#include <SD.h>
#include <AceCRC.h>
#include "AsyncJson.h"
#include "ArduinoJson.h"

#include "common.h"
#include "eeprom.h"
#include "mhz19.h"

using namespace ace_crc::crc32_nibblem;

#define SD_CS_PIN 33

#define MB (1024 * 1024)
#define GB (MB * 1024)

struct data_t;

char sdbuffer[256];

void updateSdCardStatus(bool force);

char* getSdCardType() {
  switch (sdInfo.card_type) {
    case CARD_SD: return (char *)"SD";
    case CARD_SDHC: return (char *)"SDHC";
    default: return (char *)"Unknown";
  }
}

uint32_t getLastFileName() {
  for (uint32_t i = 0; i < 1000000; i++) {
    sprintf(fileName, "/B%07d.bin", sdInfo.file_cnt);
    if (!SD.exists(fileName)) return i;
    sdInfo.file_cnt++;
  }
  return 1000000;
}

void listGetSdCardContents() {
  File dir = SD.open("/");

  for (int nr = 1; nr <= 1000000; nr++) {
    String fileName = dir.getNextFileName();

    if (fileName.length() == 0) {
      break;
    }
    Serial.println("::" + fileName);
  }
  Serial.println("::OK");
  dir.close();
}

void getBinFile() {
  char *buf = sdbuffer;

  while (Serial.available()) {
    char c = Serial.read();

    if (c >= ' ') *buf++ = c;
  }
  *buf = 0;
  if (buf == sdbuffer || sdbuffer[0] != '/') {
    Serial.println("E:Invalid filename!");
    return;
  }
  if (!SD.exists(sdbuffer)) {
    Serial.println("E:File not found!");
    return;
  }

  File file = SD.open(sdbuffer);
  int cc = 0;

  sprintf(buffer, "I:Send file %s with size %d", sdbuffer, file.size());
  Serial.println(buffer);
  while (true) {
    size_t cnt = file.read((uint8_t *)&buffer, (size_t)4096);

    if (cnt == 0) {
      break;
    }
    cc += cnt;
    sprintf(buffer, "I:%d %d", cnt, cc);
    Serial.println(buffer);
    Serial.print("::");
    for (size_t i = 0; i < cnt; i++) {
      uint8_t c = (uint8_t)buffer[i];

      if (c < 0x10) Serial.print("0");
      sprintf(buffer, "%02X", c);
      Serial.print(buffer);
    }
    Serial.println("");
  }
  file.close();
  Serial.println("::OK");
}

void deleteBinFile() {
  String fileName = "";

  while (Serial.available()) {
    char c = Serial.read();

    if (c >= ' ') fileName += c;
  }
  if (fileName.length() == 0 || fileName[0] != '/') {
    Serial.println("E:Invalid filename!");
    return;
  }
  if (SD.exists(fileName)) {
    SD.remove(fileName);
    Serial.println("::OK");
  } else {
    Serial.println("E:File not found!");
  }
}

void getGetSdCardContents(AsyncWebSocket &ws, int skip) {
  File dir = SD.open("/");
  String fileNames = "";
  uint32_t nr = 1;
  uint32_t cnt = 0;

  if (DebugMode) {
    Serial.print("I:getGetSdCardContents: skip=");
    Serial.print(skip);
  }
  for (nr = 1; nr <= 1000000; nr++) {
    String fileName = dir.getNextFileName();

    if (fileName.length() == 0) {
      break;
    }
    if (skip > 0) {
      skip--;
      continue;
    }
    if (DebugMode) {
      Serial.println(fileName);
    }
    if (cnt > 0) {
      fileNames += "\n";
    }
    fileNames += fileName;
    cnt++;
    if (cnt == 100) break;
  }
  dir.close();
  if (DebugMode) {
    Serial.print(" cnt=");
    Serial.println(cnt);
  }
  if (cnt > 0) {
    sendSdCardContentsChunk(nr - 1, cnt, fileNames, ws);
  }
  if (cnt < 100) {
    ws.textAll("{\"filename_nr\":-1}");
  }
  if (SerialOut) {
    sprintf(buffer, "I:FILES %d", nr);
    Serial.println(buffer);
  }
}

bool initSDCard() {
  if (!SD.begin(SD_CS_PIN)) {
    sdInfo.card_type = CARD_NONE;
    if (SerialOut) {
      Serial.println("E:No SD-Card detected");
    }
    return false;
  }
  sdInfo.card_type = SD.cardType();
  sdInfo.card_size = SD.cardSize();
  sdInfo.total_bytes = SD.totalBytes();
  sdInfo.used_bytes = SD.usedBytes();
  sdInfo.file_cnt = readFileCnt();
  if (SerialOut) {
    sprintf(buffer, "I:SD-Card Type=%d", sdInfo.card_type);
    Serial.println(buffer);
    sprintf(buffer, "I:FileCnt=%d", getLastFileName());
    Serial.println(buffer);
  }
  return true;
}

bool setupSDCard(int16_t y) {
  if (!initSDCard()) {
    return false;
  }
  if (!SerialOut) {
    return true;
  }
  Serial.print("I:SD-Card type:     ");
  Serial.println(getSdCardType());
  Serial.print("I:Card size:  (MB): ");
  Serial.println(sdInfo.card_size / MB);
  Serial.print("I:Volume size (MB): ");
  Serial.println(sdInfo.total_bytes / MB);
  Serial.print("I:Used size   (MB): ");
  Serial.println(sdInfo.used_bytes / MB);
  return true;
}

void updateSdCardStatus(bool force) {
  File dir = SD.open("/");
  String fileName = dir.getNextFileName();

  dir.close();
  if (fileName.length() == 0) {
    Serial.println("E:SD-Card ?");
    if (sdInfo.card_type != CARD_NONE) {
      Serial.println("E:SD-Card removed!");
    }
    SD.end();
    if (initSDCard()) {
      Serial.println("I:SD-Card inserted.");
    }
  }
}

bool writeSection(File *datFile, char* section, uint8_t *data, size_t size) {
  size_t written;

  if (size > 65536) {
    written = datFile->write(data, 65536);
    written += datFile->write(&data[65536], size - 65536);
  } else {
    written = datFile->write(data, size);
  }
  if (SerialOut) {
    sprintf(buffer, "I:Write %s %d %d", section, size, written);
    Serial.println(buffer);
  }
  return written == size;
}

bool writeDataToFile() {
  if (SerialOut) {
    Serial.println("I:writeDataToFile");
  }
  if (SD.cardType() == CARD_NONE) {
    SD.end();
    if (!initSDCard()) {
      return false;
    }
  }
  getLastFileName();
  if (SerialOut) {
    sprintf(buffer, "I:Write %d bytes to %s", sizeof(file_header_t) + sizeof(offsets) + sizeof(sdsInfo) + sizeof(data), fileName);
    Serial.println(buffer);
  }

  file_header_t fileHeader;

  fileHeader.id = 0x4D53; // ID=MS: MS=MultiSensor
  fileHeader.serial_nr = SERIAL_NR;
  fileHeader.hw_version = HW_VERSION;
  fileHeader.sw_version = SW_VERSION;
  fileHeader.data_version = DATA_VERSION;
  memcpy(fileHeader.mhz19_version, mhz19_version, 4);
  fileHeader.data_cnt = DATA_SIZE; // Number of data sets
  fileHeader.startup_cnt = startup_cnt;
  fileHeader.crc = 0;

  crc_t crc = crc_init();
  crc = crc_update(crc, &fileHeader, sizeof(fileHeader));
  crc = crc_update(crc, &offsets, sizeof(offsets));
  crc = crc_update(crc, &sdsInfo, sizeof(sdsInfo));
  crc = crc_update(crc, &data, DATA_SIZE * sizeof(data_t));
  fileHeader.crc = crc_finalize(crc);

  bool result = false;
  for (uint8_t i = 0; i < 4; i++) {
    File datFile = SD.open(fileName, FILE_WRITE, true);
    result = writeSection(&datFile, (char *)"FileHeader", (uint8_t *)&fileHeader, sizeof(fileHeader)); // 22 byte
    result &= writeSection(&datFile, (char *)"Offsets", (uint8_t *)&offsets, sizeof(offsets)); // 88 byte
    result &= writeSection(&datFile, (char *)"SdsInfo", (uint8_t *)&sdsInfo, sizeof(sdsInfo)); // 12 byte
    result &= writeSection(&datFile, (char *)"Data", (uint8_t *)data, DATA_SIZE * sizeof(data_t)); // cnt * 1821 byte
    datFile.flush();
    datFile.close();
    datFile = SD.open(fileName, FILE_READ, false);
    size_t size = datFile.size();
    datFile.close();
    if (size == 65678) break;
    if (i == 2) {
      sprintf(buffer, "E:Only %d bytes written! Giving up!", size);
      Serial.println(buffer);
    } else {
      sprintf(buffer, "W:Only %d bytes written! Retrying...", size);
      Serial.println(buffer);
    }
  }

  if (SerialOut) {
    if (result) {
      sprintf(buffer, "I:Result: %d", result);
      Serial.println(buffer);
    } else {
      Serial.println("E:Writing data to file failed");
    }
  }
  sdInfo.used_bytes = SD.usedBytes();
  sdInfo.file_cnt++;
  updateFileCnt(sdInfo.file_cnt);
  return result;
}

bool deleteDataFiles(uint8_t cnt) {
  for (uint8_t i = 0; i < cnt; i++) {
    sprintf(fileName, "/B%07d.bin", sdInfo.file_cnt);
    if (!SD.exists(fileName)) return false;
    SD.remove(fileName);
  }
  return true;
}

#endif // _SD_H
