#ifndef _DHT20_H
#define _DHT20_H

#include <DFRobot_DHT20.h>

#include "common.h"

// Interface is I2C

DFRobot_DHT20 dht20;

bool setupDHT20(int16_t y) {
  while (dht20.begin()) {
    if (SerialOut) {
      Serial.println("E:Initialize sensor failed");
    }
    delay(1000);
  }
  return true;
}

void updateDHT20(dht20_t *p) {
  temp.dht20 = dht20.getTemperature();
  humidity.dht20 = dht20.getHumidity() * 100.0;
  p->temp = lround(temp.dht20 * 100.0); // °C
  p->humidity = lround(humidity.dht20 * 10.0); // %
  temp.dht20 +=  offsets.dht20_temp;
  humidity.dht20 += offsets.dht20_humidity;
}

#endif // _DHT20_H
