#ifndef _IMU9DOF_H
#define _IMU9DOF_H

#include <Wire.h>
#include <AK09918.h>
#include <ICM20600.h>

#include "float16.h"

#define IMU9DOF_CAL_SAMPLE_CNT 5000

AK09918 ak09918;
ICM20600 icm20600(true);

int16_t acc_x, acc_y, acc_z;
int32_t offset_x, offset_y, offset_z;
float roll, pitch;
// Find the magnetic declination at your location
// http://www.magnetic-declination.com/
float declination_shenzhen = -2.2;


bool setupIMU9DOF(int16_t y) {
  // join I2C bus (I2Cdev library doesn't do this automatically)
  Wire.begin();

  icm20600.initialize();
  ak09918.initialize();
  //ak09918.switchMode(AK09918_POWER_DOWN);
  ak09918.switchMode(AK09918_CONTINUOUS_20HZ);

  AK09918_err_type_t err = ak09918.isDataReady();

  for (int i = 0; i < 5; i++) {
    if (err == AK09918_ERR_OK) return true;
    Serial.println("I:Waiting Sensor");
    delay(100);
    err = ak09918.isDataReady();
  }
  return false;
}

uint8_t resetIMU9DOF() {
  uint8_t result = (uint8_t)ak09918.reset();

  icm20600.reset();
  delay(100);
  ak09918.switchMode(AK09918_CONTINUOUS_20HZ);
  icm20600.initialize();
  return result;
}

void updateICM20600(imu9dof_t *p) {
  if (p->icm20600_cnt >= ICM20600_SIZE) {
    return;
  }

  xyz_t *xyz = &p->accel[p->icm20600_cnt];
  xyz->x = f32tof16(icm20600.getAccelerationX()); // [mg]
  xyz->y = f32tof16(icm20600.getAccelerationY());
  xyz->z = f32tof16(icm20600.getAccelerationZ());

  xyz = &p->gyro[p->icm20600_cnt];
  xyz->x = f32tof16(icm20600.getGyroscopeX()); // [DEG/s]
  xyz->y = f32tof16(icm20600.getGyroscopeY());
  xyz->z = f32tof16(icm20600.getGyroscopeZ());

  temp.icm20600 = (float)icm20600.getTemperatureRaw() / 326.8 + 25.0;
  p->temp = lround(temp.icm20600 * 100.0); // °C
  temp.icm20600 += offsets.imu9dof_temp;

  p->icm20600_cnt++;
}

void updateAK09918(imu9dof_t *p) {
  if (p->ak09918_cnt >= AK09918_SIZE) {
    return;
  }

  int32_t x, y, z;
  xyz_t *xyz = &p->magnet[p->ak09918_cnt];

  AK09918_err_type_t error = ak09918.getData(&x, &y, &z); // [uT]

  xyz->x = f32tof16((float)x);
  xyz->y = f32tof16((float)y);
  xyz->z = f32tof16((float)z);
  p->ak09918_error = error;
  p->ak09918_cnt++;
}

void updateIMU9DOF(imu9dof_t *p) {
  updateICM20600(p);
  updateAK09918(p);

  // roll/pitch in radian
  /*roll = atan2((float)acc_y, (float)acc_z);
  pitch = atan2(-(float)acc_x, sqrt((float)acc_y * acc_y + (float)acc_z * acc_z));
  Serial.print("Roll: ");
  Serial.println(roll * 57.3);
  Serial.print("Pitch: ");
  Serial.println(pitch * 57.3);

  double Xheading = x * cos(pitch) + y * sin(roll) * sin(pitch) + z * cos(roll) * sin(pitch);
  double Yheading = y * cos(roll) - z * sin(pitch);

  double heading = 180 + 57.3 * atan2(Yheading, Xheading) + declination_shenzhen;*/
}

#endif // _IMU9DOF_H
