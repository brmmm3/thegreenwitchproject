#ifndef _TLV493D_H
#define _TLV493D_H

#include <Tlv493d.h>

#include "float16.h"
#include "common.h"

// Interface is I2C
 
Tlv493d tlv493dSensor = Tlv493d();

bool setupTLV493D(int16_t y) {
  tlv493dSensor.begin();
  return true;
}

void updateTLV493D(tlv493d_t *p) {
  uint8_t cnt = p->cnt;

  tlv493dSensor.updateData();
  if (cnt >= TLV493D_SIZE) {
    return;
  }

  xyz_t *xyz = &p->magnet[cnt];

  xyz->x = f32tof16(tlv493dSensor.getX()); // mT
  xyz->y = f32tof16(tlv493dSensor.getY());
  xyz->z = f32tof16(tlv493dSensor.getZ());

  temp.tlv493d = tlv493dSensor.getTemp();
  p->temp = lround(temp.tlv493d * 100.0); // °C
  temp.tlv493d +=  offsets.tlv493d_temp;

  p->cnt++;
}

#endif // _TLV493D_H
