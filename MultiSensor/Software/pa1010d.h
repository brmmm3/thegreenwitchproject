#ifndef _PA1010D_H
#define _PA1010D_H

#include <Adafruit_GPS.h>

// Connect to the GPS on the hardware I2C port
Adafruit_GPS pa1010d(&Wire);

bool setupPA1010D(int16_t y) {
  // 9600 NMEA is the default baud rate for Adafruit MTK GPS's- some use 4800
  pa1010d.begin(0x10);  // The I2C address to use is 0x10
  // uncomment this line to turn on RMC (recommended minimum) and GGA (fix data) including altitude
  //GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);
  // uncomment this line to turn on only the "minimum recommended" data
  //GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCONLY);
  pa1010d.sendCommand(PMTK_SET_NMEA_OUTPUT_ALLDATA);
  // For parsing data, we don't suggest using anything but either RMC only or RMC+GGA since
  // the parser doesn't care about other sentences at this time
  // Set the update rate
  pa1010d.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ); // 1 Hz update rate
  // For the parsing code to work nicely and have time to sort thru the data, and
  // print it out we don't suggest using anything higher than 1 Hz

  // Request updates on antenna status, comment out to keep quiet
  pa1010d.sendCommand(PGCMD_ANTENNA);

  // Ask for firmware version
  pa1010d.println(PMTK_Q_RELEASE);
  return true;
}

void resetPA1010D() {
  setupPA1010D(0);
}

void readPA1010D(gps_t *p) {
  p->date.year = 2000 + (uint16_t)pa1010d.year;
  p->date.month = pa1010d.month;
  p->date.day = pa1010d.day;
  p->time.hour = pa1010d.hour;
  p->time.minute = pa1010d.minute;
  p->time.second = pa1010d.seconds;
  p->satellites = pa1010d.satellites;
  p->latitude = pa1010d.latitudeDegrees;
  p->longitude = pa1010d.longitudeDegrees;
  p->altitude = lround(pa1010d.altitude * 10.0);
  p->speed = lround(pa1010d.speed * 10.0);
  p->angle = pa1010d.angle;
  p->hdop = pa1010d.HDOP;
  p->vdop = pa1010d.VDOP;
  p->pdop = pa1010d.PDOP;
  p->valid = pa1010d.fix;
  p->quality = pa1010d.fixquality;
  p->quality_3d = pa1010d.fixquality_3d;
}

void updatePA1010D(gps_t *p) {

  unsigned long now = millis();

  for (uint8_t i = 0; i < 100; i++) {
    pa1010d.read();
    // if (SerialOut) Serial.print(c);
    // if a sentence is received, we can check the checksum, parse it...
    if (pa1010d.newNMEAreceived()) {
      // a tricky thing here is if we print the NMEA sentence, or data
      // we end up not listening and catching other sentences!
      // so be very wary if using OUTPUT_ALLDATA and trying to print out data
      //Serial.print(pa1010d.lastNMEA()); // this also sets the newNMEAreceived() flag to false
      if (pa1010d.parse(pa1010d.lastNMEA())) { // this also sets the newNMEAreceived() flag to false
        readPA1010D(p); // we can fail to parse a sentence in which case we should just wait for another
      }
      break;
    }
    if (millis() - now > 10) {
      break;
    }
  }
}

#endif // _PA1010D_H
