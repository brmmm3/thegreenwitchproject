#ifndef _EEPROM_H
#define _EEPROM_H

#include "eeprom_24c32.h"

#define EEPROM_ADDRESS 0x50

/* EEPROM address table
  0-51   Operating time valid (AA).
  52-101 Operating time in MINUTES. Counter is 2 bytes wide. After reaching 65535 the next 2 bytes are used.
           This allows up to 15 years of usage.
  127 - 163 Offsets
*/

#define EEP_CNT_SIZE    50

#define EEP_OP_TIME     0     // Each tick is 5min
#define EEP_FILE_CNT    EEP_CNT_SIZE + 2
#define EEP_OFFSETS     127
#define EEP_STARTUP_CNT 130 + sizeof(offsets)

static Eeprom24C32 eeprom(EEPROM_ADDRESS);

void writeEEPStartupCnt(uint16_t cnt, bool init) {
  eeprom.writeBytes(EEP_STARTUP_CNT + 1, sizeof(cnt), (uint8_t *)&cnt);
  delay(10);
  if (init) {
    eeprom.writeByte(EEP_STARTUP_CNT, 0xab);
    delay(10);
  }
}

uint16_t readEEPStartupCnt() {
  uint16_t cnt = 0;

  if (eeprom.readByte(EEP_STARTUP_CNT) == 0xab) {
    eeprom.readBytes(EEP_STARTUP_CNT + 1, sizeof(cnt), (uint8_t *)&cnt);
    cnt++;
    writeEEPStartupCnt(cnt, false);
  } else {
    // Clear if not valid
    writeEEPStartupCnt(cnt, true);
  }
  return cnt;
}

void writeOffsets() {
  eeprom.writeBytes(EEP_OFFSETS + 1, sizeof(offsets), (uint8_t *)&offsets);
  delay(10);
  eeprom.writeByte(EEP_OFFSETS, 0xa6);
  delay(10);
}

void readOffsets() {
  if (eeprom.readByte(EEP_OFFSETS) == 0xa6) {
    Serial.println("I:Offsets VALID");
    eeprom.readBytes(EEP_OFFSETS + 1, sizeof(offsets), (uint8_t *)&offsets);
  } else {
    // Clear if not valid
    Serial.println("I:Offsets INIT");
    memset(&offsets, 0, sizeof(offsets));
    writeOffsets();
  }
}

uint32_t readEEPCounter(uint16_t address, uint8_t size) {
  if (eeprom.readByte(address) != 0xa5) {
    sprintf(buffer, "I:readEEPCounter INIT %d", address);
    Serial.println(buffer);
    memset(buffer, 0, size + 1);
    buffer[0] = 0xa5;
    eeprom.writeBytes(address, size + 1, (uint8_t *)&buffer);
    delay(10);
    return 0;
  }

  uint16_t value;
  uint32_t total = 0;

  for (uint16_t i = address + 1; i <= address + size; i += 2) {
    eeprom.readBytes(i, 2, (uint8_t *)&value);
    total += value;
    if (value < 0xffff) {
      break;
    }
  }
  return total;
}

void writeEEPCounter(uint16_t address, uint32_t total, uint8_t size) {
  uint16_t value;

  for (uint16_t i = address + 1; i <= address + size; i += 2) {
    eeprom.readBytes(i, 2, (uint8_t *)&value);
    if (value < 0xffff) {
      if (total < 0xffff) {
        value = total;
        eeprom.writeBytes(i, 2, (uint8_t *)&value);
        delay(10);
        break;
      } else {
        value = 0xffff;
        eeprom.writeBytes(i, 2, (uint8_t *)&value);
        delay(10);
      }
    }
    total -= 0xffff;
  }
}

uint32_t readFileCnt() {
  return readEEPCounter(EEP_FILE_CNT, EEP_CNT_SIZE);
}

void updateFileCnt(uint32_t file_cnt) {
  writeEEPCounter(EEP_FILE_CNT, file_cnt, EEP_CNT_SIZE);
}

void updateOperatingTime() {
  writeEEPCounter(EEP_OP_TIME, operating_time, EEP_CNT_SIZE);
}

void setupEEPROM() {
  eeprom.initialize();
  readOffsets();
  startup_cnt = readEEPStartupCnt();
  operating_time = readEEPCounter(EEP_OP_TIME, EEP_CNT_SIZE);
} 

#endif // _EEPROM_H
