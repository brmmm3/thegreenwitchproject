// GM102B (NO2)
// Rs means resistance of sensor in 2ppm NO2 under different temp. and humidity.
// Rso means resistance of the sensor in 2ppm NO2 under 20°C/55%RH.

const gm102b_rh_offset = [
    [ -10.0, 0.0, 10.0, 20.0, 30.0, 40.0, 50.0 ], // °C
    [ 1.71, 1.58, 1.45, 1.39, 1.12, 1.00, 0.89 ], // Rs/R0 @ 30%RH
    [ 1.49, 1.32, 1.28, 1.08, 0.99, 0.88, 0.71 ], // Rs/R0 @ 60%RH
    [ 1.28, 1.15, 10.9, 0.90, 0.86, 0.71, 0.68 ]  // Rs/R0 @ 85%RH
];

const gm102b_u2gas = [
    [ 0.0, 0.21, 0.39, 0.7, 0.95, 1.15, 1.35, 1.45, 1.6, 1.69, 1.79, 1.81 ], // V
    [ 0.0, 0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0 ], // NO2 [ppm]
];

// GM302B (Ethanol=C2H5OH)
// Rs means resistance of sensor in 50ppm ethanol under different temp. and humidity.
// Rso means resistance of the sensor in 50ppm ethanol under 20°C/65%RH.

const gm302b_rh_offset = [
    [ -10.0, -5.0, 0.0, 5.0, 10.0, 15.0, 20.0, 25.0, 30.0, 35.0, 40.0, 45.0, 50.0 ],  // °C
    [ 1.71, 1.61, 1.58, 1.50, 1.42, 1.30, 1.25, 1.18, 1.15, 1.12, 1.00, 0.92, 0.88 ], // Rs/R0 @ 30%RH
    [ 1.45, 1.36, 1.33, 1.28, 1.20, 1.11, 1.08, 1.00, 0.98, 0.95, 0.85, 0.79, 0.73 ], // Rs/R0 @ 60%RH
    [ 1.27, 1.20, 1.18, 1.10, 1.05, 0.95, 0.92, 0.88, 0.86, 0.81, 0.72, 0.69, 0.64 ]  // Rs/R0 @ 85%RH
];

const gm302b_u2gas = [
    [ 1.25, 1.5, 2.0, 2.25, 2.5, 3.1, 3.3, 3.6, 3.7, 3.8, 3.85 ], // Alcohol/Ethanol [V]
    [ 0.0, 1.0, 3.5, 5.0, 10.0, 30.0, 50.0, 80.0, 100.0, 200.0, 500.0 ] // VOC [ppm]
];

// GM502B (VOC)
// Rs means resistance of sensor in 150ppm CO gas under different temp. and humidity.
// Rso means resistance of the sensor in 150ppm CO gas under 20°C/55%RH.

const gm502b_rh_offset = [
    [ -10.0, -5.0, 0.0, 5.0, 10.0, 15.0, 20.0, 25.0, 30.0, 35.0, 40.0, 45.0, 50.0 ],  // °C
    [ 1.71, 1.62, 1.54, 1.50, 1.42, 1.30, 1.25, 1.16, 1.14, 1.11, 1.00, 0.92, 0.88 ], // Rs/R0 @ 30%RH
    [ 1.45, 1.38, 1.35, 1.28, 1.21, 1.11, 1.08, 1.00, 0.98, 0.96, 0.85, 0.79, 0.75 ], // Rs/R0 @ 60%RH
    [ 1.25, 1.20, 1.18, 1.10, 1.05, 0.95, 0.92, 0.88, 0.86, 0.81, 0.73, 0.68, 0.62 ]  // Rs/R0 @ 85%RH
];

const gm502b_u2gas = [
    [ 2.52, 2.90, 3.20, 3.40, 3.60, 3.90, 4.05, 4.15, 4.20 ], // Alcohol [V]
    [ 0.0, 1.0, 3.5, 5.0, 10.0, 30.0, 50.0, 80.0, 100.0 ]  // VOC [ppm]
];

// GM702B (CO)
// Rs means resistance of sensor in 150ppm CO gas under different temp. and humidity.
// Rso means resistance of the sensor in 150ppm CO gas under 20°C/55%RH

const gm702b_rh_offset = [
    [ -10.0, 0.0, 10.0, 20.0, 30.0, 40.0, 50.0 ], // °C
    [ 1.71, 1.58, 1.45, 1.38, 1.13, 1.01, 0.88 ], // Rs/R0 @ 30%RH
    [ 1.47, 1.32, 1.28, 1.08, 0.98, 0.88, 0.72 ], // Rs/R0 @ 60%RH
    [ 1.28, 1.15, 1.08, 0.90, 0.87, 0.71, 0.68 ]  // Rs/R0 @ 85%RH
];

const gm702b_u2gas = [
    [ 0.25, 0.65, 0.98, 1.35, 1.8, 1.98, 2.1, 2.38, 2.42 ], // V
    [ 0.0, 5.0, 10.0, 20.0, 50.0, 100.0, 160.0, 500.0, 1000.0 ]  // CO [ppm]
];

const GM_VERF = 3.3;
const GM_RESOLUTION = 1023.0;


function calcVol(adc) {
    return (adc * GM_VERF) / GM_RESOLUTION;
};

function u_corr_rh(u, temp, humidity, u_corr, size) {
    var hum_idx1;
    var hum_idx2;
    var ref_hum1, ref_hum2;
    if (humidity <= 30.0) {
        hum_idx1 = 1;
        hum_idx2 = 1;
        ref_hum1 = 30.0;
        ref_hum2 = 60.0;
    } else if (humidity <= 60.0) {
        hum_idx1 = 1;
        hum_idx2 = 2;
        ref_hum1 = 30.0;
        ref_hum2 = 60.0;
    } else if (humidity <= 85.0) {
        hum_idx1 = 2;
        hum_idx2 = 3;
        ref_hum1 = 60.0;
        ref_hum2 = 85.0;
    } else {
        hum_idx1 = 3;
        hum_idx2 = 3;
        ref_hum1 = 60.0;
        ref_hum2 = 85.0;
    }
    // First get Rs/R0
    var old_rsr01 = u_corr[hum_idx1][0];
    var old_rsr02 = u_corr[hum_idx2][0];
    var rsr01 = old_rsr01;
    var rsr02 = old_rsr02;
    var old_temp = u_corr[0][0];
    if (temp >= old_temp) {
        for (var i = 1; i < size; i++) {
            var new_temp = u_corr[0][i];
            rsr01 = u_corr[hum_idx1][i];
            rsr02 = u_corr[hum_idx2][i];
            if (temp <= new_temp) {
                old_rsr01 += (temp - old_temp) / (new_temp - old_temp) * (rsr01 - old_rsr01);
                old_rsr02 += (temp - old_temp) / (new_temp - old_temp) * (rsr02 - old_rsr02);
                break;
            }
            old_temp = new_temp;
            old_rsr01 = rsr01;
            old_rsr02 = rsr02;
        }
    }
    var fact = (old_rsr01 + (humidity - ref_hum1) / (ref_hum2 - ref_hum1) * (old_rsr02 - old_rsr01));
    return u / fact;
}

function u2ppm(u, u2gas, size) {
    var old_ppm = u2gas[1][0];
    var old_u = u2gas[0][0];
    if (u <= old_u) return old_ppm;
    for (var i = 1; i < size; i++) {
        var new_u = u2gas[0][i];
        var ppm = u2gas[1][i];
        if (u <= new_u) {
            return old_ppm + (u - old_u) / (new_u - old_u) * (ppm - old_ppm);
        }
        old_u = new_u;
        old_ppm = ppm;
    }
    return old_ppm;
}

export function getNO2ppm(raw, temp, humidity) {
    var no2_u = calcVol(raw);
    var no2_corr = u_corr_rh(no2_u, temp, humidity, gm102b_rh_offset, 7);
    return u2ppm(no2_corr, gm102b_u2gas, 12);
}

export function getC2H5OHppm(raw, temp, humidity) {
    var c2h5oh_u = calcVol(raw);
    var c2h5oh_corr = u_corr_rh(c2h5oh_u, temp, humidity, gm302b_rh_offset, 13);
    return u2ppm(c2h5oh_corr, gm302b_u2gas, 11);
}

export function getVOCppm(raw, temp, humidity) {
    var voc_u = calcVol(raw);
    var voc_corr = u_corr_rh(voc_u, temp, humidity, gm502b_rh_offset, 13);
    return u2ppm(voc_corr, gm502b_u2gas, 9);
}

export function getCOppm(raw, temp, humidity) {
    var co_u = calcVol(raw);
    var co_corr = u_corr_rh(co_u, temp, humidity, gm702b_rh_offset, 7);
    return u2ppm(co_corr, gm702b_u2gas, 9);
}
