use std::collections::{HashMap, HashSet};
use std::path::{Path, PathBuf};

use pyo3::prelude::*;

use crate::file::BinFile;
use crate::types::DATA_SIZE;

fn cleanup(paths2remove: Vec<String>, errors: &mut HashMap<String, String>) -> PyResult<()> {
    for path in paths2remove.iter() {
        if let Err(err) = std::fs::remove_file(&path) {
            errors.insert(path.to_owned(), err.to_string());
        }
    }
    let dirs2remove: HashSet<String> = paths2remove
        .iter()
        .map(|x| Path::new(x).parent().unwrap().to_str().unwrap().to_string())
        .collect();
    for dir in dirs2remove {
        match std::fs::read_dir(&dir) {
            Ok(mut entry) => {
                if entry.next().is_none() {
                    std::fs::remove_dir(dir)?;
                }
            }
            Err(_) => {}
        }
    }
    Ok(())
}

#[pyfunction]
#[pyo3(signature = (path, force=None))]
pub fn load_bin_file(path: PathBuf, force: Option<bool>, py: Python) -> PyResult<BinFile> {
    BinFile::new(Some(path.clone()), force, py)
}

#[pyfunction]
#[pyo3(signature = (paths, force=None))]
pub fn load_bin_files(
    paths: Vec<PathBuf>,
    force: Option<bool>,
    py: Python,
) -> PyResult<(HashMap<String, BinFile>, HashMap<String, String>)> {
    let mut files = HashMap::new();
    let mut errors = HashMap::new();
    for path in paths {
        let path_str = path.to_str().unwrap().to_string();
        match BinFile::new(Some(path), force, py) {
            Ok(file) => {
                files.insert(path_str, file);
            }
            Err(err) => {
                errors.insert(path_str, err.to_string());
            }
        }
    }
    Ok((files, errors))
}

#[pyfunction]
#[pyo3(signature = (paths, out_dir=None, compact=None, force=None, remove_bin_files=None))]
pub fn merge_bin_files(
    mut paths: Vec<PathBuf>,
    out_dir: Option<PathBuf>, // If defined write merged bin files to this directory as record files
    compact: Option<bool>,    // Compact mode for arrays
    force: Option<bool>,      // Force merging
    remove_bin_files: Option<bool>, // Remove merged and buggy bin files
    py: Python,
) -> PyResult<(HashMap<String, BinFile>, HashMap<String, String>)> {
    let compact = compact.unwrap_or(false);
    let force = force.unwrap_or(false);
    let remove_bin_files = remove_bin_files.unwrap_or(false);
    let (mut files, mut errors) = load_bin_files(paths.clone(), Some(force), py)?;
    if remove_bin_files {
        // Remove all buggy files
        cleanup(errors.keys().map(|x| x.to_owned()).collect(), &mut errors)?;
    }
    let mut records = HashMap::new();
    if files.is_empty() {
        return Ok((records, errors));
    }
    paths.sort();
    let mut record = BinFile::new(None, Some(force), py)?;
    let mut remove = Vec::new();
    // Get first valid file with data
    while !files.is_empty() {
        let path = paths.remove(0);
        let file = files.remove(&path.to_str().unwrap().to_string());
        if let Some(file) = file {
            remove.push(path.to_str().unwrap().to_string());
            if !file.data.is_empty() {
                record = file;
                record.into_record();
                record.bin_files.push(path.to_str().unwrap().to_string());
                break;
            }
        }
    }
    while !files.is_empty() {
        let path = paths.remove(0);
        let file = files.remove(&path.to_str().unwrap().to_string());
        if let Some(file) = file {
            remove.push(path.to_str().unwrap().to_string());
            let last_data = &record.data[record.data.len() - 1];
            let delta_cnt = file.data[0].counter as i32 - last_data.counter as i32;
            let reason_serial_nr = record.file_header.serial_nr != file.file_header.serial_nr;
            let reason_data_version =
                record.file_header.data_version != file.file_header.data_version;
            let reason_delta_cnt = !force && (delta_cnt < 1 || delta_cnt > DATA_SIZE as i32);
            let reason_date_time = record._date_time(last_data) > record._date_time(&file.data[0]);
            if reason_serial_nr || reason_data_version || reason_delta_cnt || reason_date_time {
                if let Some(ref out_dir) = out_dir {
                    record.write(out_dir.join(record._filename()), compact, py)?;
                }
                records.insert(record._filename(), record.clone());
                record = file;
                record.into_record();
                record.bin_files.push(path.to_str().unwrap().to_string());
                record.comment = format!(
                    "Split reason:{}{}{}{}\n\n{}:\n{}",
                    match reason_serial_nr {
                        true => " SerialNr",
                        false => "",
                    },
                    match reason_data_version {
                        true => " DataVersion",
                        false => "",
                    },
                    match reason_delta_cnt {
                        true => " DeltaCnt",
                        false => "",
                    },
                    match reason_date_time {
                        true => " DateTime",
                        false => "",
                    },
                    path.to_str().unwrap().to_string(),
                    record.comment
                );
            } else {
                record.data.extend_from_slice(&file.data);
                record.file_header.data_cnt += file.file_header.data_cnt;
                record.bin_files.push(path.to_str().unwrap().to_string());
                if !file.comment.is_empty() {
                    record.comment += &format!(
                        "\n\n{}:\n{}",
                        path.to_str().unwrap().to_string(),
                        file.comment
                    );
                }
            }
        }
    }
    if !record.data.is_empty() {
        if let Some(ref out_dir) = out_dir {
            record.write(out_dir.join(record._filename()), compact, py)?;
        }
        records.insert(record._filename(), record);
    }
    if remove_bin_files {
        // Remove all valid files
        cleanup(remove, &mut errors)?;
    }
    Ok((records, errors))
}

#[pyclass]
#[derive(Debug)]
pub struct Records {
    #[pyo3(get)]
    pub records: HashMap<String, BinFile>,
    #[pyo3(get)]
    pub out_dir: Option<PathBuf>,
    #[pyo3(get)]
    pub errors: HashMap<String, String>,
}

#[pymethods]
impl Records {
    #[new]
    pub fn new(
        paths: Option<Vec<PathBuf>>,
        out_dir: Option<PathBuf>,
        compact: Option<bool>,
        force: Option<bool>,
        remove_bin_files: Option<bool>, // Remove merged and buggy bin files
        py: Python,
    ) -> PyResult<Self> {
        let records;
        let errors;
        if let Some(mut paths) = paths {
            paths.sort();
            (records, errors) =
                merge_bin_files(paths, out_dir.clone(), compact, force, remove_bin_files, py)?;
        } else {
            records = HashMap::new();
            errors = HashMap::new();
        }
        Ok(Records {
            records,
            out_dir,
            errors,
        })
    }

    pub fn clear(&mut self) {
        self.errors.clear();
        self.records.clear();
    }

    pub fn clear_errors(&mut self) {
        self.errors.clear();
    }

    pub fn clear_records(&mut self) {
        self.records.clear();
    }

    pub fn merge_bin_files(
        &mut self,
        mut paths: Vec<PathBuf>,
        compact: Option<bool>,
        force: Option<bool>,
        remove_bin_files: Option<bool>, // Remove merged and buggy bin files
        py: Python,
    ) -> PyResult<()> {
        paths.sort();
        let (records, errors) = merge_bin_files(
            paths,
            self.out_dir.clone(),
            compact,
            force,
            remove_bin_files,
            py,
        )?;
        self.records.extend(records);
        self.errors.extend(errors);
        Ok(())
    }

    pub fn load_record_file(
        &mut self,
        path: PathBuf,
        force: Option<bool>,
        py: Python,
    ) -> PyResult<()> {
        self.records.insert(
            path.to_str().unwrap().to_string(),
            BinFile::new(Some(path), force, py)?,
        );
        Ok(())
    }

    pub fn load_record_files(
        &mut self,
        mut paths: Vec<PathBuf>,
        force: Option<bool>,
        py: Python,
    ) -> PyResult<()> {
        paths.sort();
        for path in paths {
            if let Err(err) = self.load_record_file(path.clone(), force, py) {
                self.errors
                    .insert(path.to_str().unwrap().to_string(), err.to_string());
            }
        }
        Ok(())
    }
}
