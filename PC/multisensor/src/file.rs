use std::fs;
use std::io::{Read, Write};
use std::path::PathBuf;

use chrono::naive::NaiveDate;
use chrono::offset::Utc;
use chrono::{DateTime, Duration};
use crc32fast::Hasher;
use xxhash_rust::xxh3::Xxh3;
use zstd;

use pyo3::exceptions;
use pyo3::prelude::*;

use crate::types::*;

const MS_FILE: u16 = 0x4d53; // MultiSensor BIN file with max size arrays
const MR_FILE: u16 = 0x4d52; // MultiSensor RECORD file (this file type is compressed) with max size arrays
const MSC_FILE: u16 = 0x6d53; // MultiSensor BIN file with compact array
const MRC_FILE: u16 = 0x6d52; // MultiSensor RECORD file (this file type is compressed) with compact array
const ZSTD_FILE: u16 = 0xb528;
const RECORD_FILE_SIZE_MAX: usize = 16777216; // 16MB
const FILE_TYPES: [u16; 4] = [MSC_FILE, MS_FILE, MRC_FILE, MR_FILE];

#[pyclass]
#[derive(Debug, Clone)]
pub struct SdsInfoT {
    #[pyo3(get)]
    pub valid: bool,
    #[pyo3(get)]
    pub sds_info_id: u32, // SDS0
    #[pyo3(get)]
    pub date: MsDateT,
    #[pyo3(get)]
    pub active: bool, // Active mode?
    #[pyo3(get)]
    pub period: u8, // Working period
    #[pyo3(get)]
    pub available: u8, // Available bytes on serial port
}

impl SdsInfoT {
    pub fn size() -> usize {
        12
    }
}

#[pymethods]
impl SdsInfoT {
    #[new]
    pub fn new() -> PyResult<Self> {
        Ok(SdsInfoT {
            valid: false,
            sds_info_id: 0,
            date: MsDateT::new(None)?,
            active: false,
            period: 0,
            available: 0,
        })
    }

    pub fn from_bytes(&mut self, buffer: &[u8]) -> PyResult<()> {
        self.valid = buffer[0] != 0;
        self.sds_info_id = u32::from_le_bytes(buffer[1..5].try_into()?);
        self.date.from_bytes(&buffer[5..])?;
        self.valid = buffer[9] != 0;
        self.period = buffer[10];
        self.available = buffer[11];
        Ok(())
    }

    pub fn to_bytes(&mut self) -> Vec<u8> {
        let mut buffer: Vec<u8> = Vec::new();
        buffer.push(self.valid as u8);
        buffer.extend_from_slice(&self.sds_info_id.to_le_bytes());
        buffer.extend_from_slice(&self.date.to_bytes());
        buffer.push(self.active as u8);
        buffer.push(self.period);
        buffer.push(self.available);
        buffer
    }

    fn __str__(&self) -> PyResult<String> {
        Ok(format!("{:?}", self))
    }

    fn __repr__(&self) -> PyResult<String> {
        self.__str__()
    }
}

#[pyclass]
#[derive(Debug, Clone)]
pub struct BinFileHeaderT {
    #[pyo3(get)]
    pub id: u16, // Identifier: MS(0x4d53)=MultiSensor BIN file, MR(0x4d52)=MultiSensor RECORD file
    #[pyo3(get)]
    pub serial_nr: u32,
    #[pyo3(get)]
    pub hw_version: u8,
    #[pyo3(get)]
    pub sw_version: u8,
    #[pyo3(get)]
    pub data_version: u8,
    #[pyo3(get)]
    pub mhz19_version: u32,
    #[pyo3(get)]
    pub data_cnt: usize, // Number of data sets
    #[pyo3(get)]
    pub startup_cnt: u32,
    #[pyo3(get)]
    pub crc: u64, // CRC of data sets
}

impl BinFileHeaderT {
    pub fn size(id: u16, data_version: u8) -> usize {
        if id == MSC_FILE || id == MS_FILE {
            if data_version > 6 {
                22
            } else {
                18
            }
        } else {
            if data_version > 6 {
                29
            } else {
                25
            }
        }
    }
}

#[pymethods]
impl BinFileHeaderT {
    #[new]
    pub fn new() -> Self {
        BinFileHeaderT {
            id: 0,
            serial_nr: 0,
            hw_version: 0,
            sw_version: 0,
            data_version: 0,
            mhz19_version: 0,
            data_cnt: 0,
            startup_cnt: 0,
            crc: 0,
        }
    }

    pub fn from_bytes(&mut self, buffer: &[u8]) -> PyResult<()> {
        self.id = u16::from_le_bytes(buffer[0..2].try_into()?);
        self.serial_nr = u32::from_le_bytes(buffer[2..6].try_into()?);
        self.hw_version = buffer[6];
        self.sw_version = buffer[7];
        self.data_version = buffer[8];
        if self.data_version > 6 {
            self.mhz19_version = u32::from_le_bytes(buffer[9..13].try_into()?);
            if self.id == MRC_FILE || self.id == MR_FILE {
                self.data_cnt = u32::from_le_bytes(buffer[13..17].try_into()?) as usize;
                self.startup_cnt = u32::from_le_bytes(buffer[17..21].try_into()?);
                self.crc = u64::from_le_bytes(buffer[21..29].try_into()?);
            } else {
                self.data_cnt = buffer[13] as usize;
                self.startup_cnt = u32::from_le_bytes(buffer[14..18].try_into()?);
                self.crc = u32::from_le_bytes(buffer[18..22].try_into()?) as u64;
            }
        } else {
            if self.id == MRC_FILE || self.id == MR_FILE {
                self.data_cnt = u32::from_le_bytes(buffer[9..13].try_into()?) as usize;
                self.startup_cnt = u32::from_le_bytes(buffer[13..17].try_into()?);
                self.crc = u64::from_le_bytes(buffer[17..25].try_into()?);
            } else {
                self.data_cnt = buffer[9] as usize;
                self.startup_cnt = u32::from_le_bytes(buffer[10..14].try_into()?);
                self.crc = u32::from_le_bytes(buffer[14..18].try_into()?) as u64;
            }
        };
        Ok(())
    }

    pub fn to_bytes(&mut self, reset_crc: bool) -> Vec<u8> {
        let mut buffer = Vec::new();
        buffer.extend_from_slice(&self.id.to_le_bytes());
        buffer.extend_from_slice(&self.serial_nr.to_le_bytes());
        buffer.push(self.hw_version);
        buffer.push(self.sw_version);
        buffer.push(self.data_version);
        if self.data_version > 6 {
            buffer.extend_from_slice(&self.mhz19_version.to_le_bytes());
        }
        if self.id == MRC_FILE || self.id == MR_FILE {
            buffer.extend_from_slice(&(self.data_cnt as u32).to_le_bytes());
            buffer.extend_from_slice(&self.startup_cnt.to_le_bytes());
            if reset_crc {
                self.crc = 0;
            }
            buffer.extend_from_slice(&self.crc.to_le_bytes());
        } else {
            buffer.push(self.data_cnt as u8);
            buffer.extend_from_slice(&self.startup_cnt.to_le_bytes());
            if reset_crc {
                self.crc = 0;
            }
            buffer.extend_from_slice(&(self.crc as u32).to_le_bytes());
        }
        buffer
    }

    fn __str__(&self) -> PyResult<String> {
        Ok(format!("{:?}", self))
    }

    fn __repr__(&self) -> PyResult<String> {
        self.__str__()
    }
}

#[pyclass]
#[derive(Debug, Clone)]
pub struct SdInfoT {
    #[pyo3(get)]
    pub card_type: u8,
    #[pyo3(get)]
    pub card_size: u64,
    #[pyo3(get)]
    pub total_bytes: u64,
    #[pyo3(get)]
    pub used_bytes: u64,
    #[pyo3(get)]
    pub file_cnt: u32,
}

impl SdInfoT {
    pub fn size() -> usize {
        29
    }
}

#[pymethods]
impl SdInfoT {
    #[new]
    pub fn new() -> PyResult<Self> {
        Ok(SdInfoT {
            card_type: 0,
            card_size: 0,
            total_bytes: 0,
            used_bytes: 0,
            file_cnt: 0,
        })
    }

    fn __str__(&self) -> PyResult<String> {
        Ok(format!("{:?}", self))
    }

    fn __repr__(&self) -> PyResult<String> {
        self.__str__()
    }
}

#[pyclass]
#[derive(Debug, Clone)]
pub struct BinFile {
    #[pyo3(get)]
    pub path: Option<PathBuf>,
    #[pyo3(get)]
    pub file_header: BinFileHeaderT,
    #[pyo3(get)]
    pub offsets: OffsetsT,
    #[pyo3(get)]
    pub sds_info: SdsInfoT,
    #[pyo3(get)]
    pub data: Vec<DataT>,
    #[pyo3(get)]
    pub bin_files: Vec<String>,
    #[pyo3(get, set)]
    pub comment: String,
}

impl BinFile {
    pub fn _filename(&self) -> String {
        let prefix = match self.file_header.id == MRC_FILE || self.file_header.id == MR_FILE {
            false => "F",
            true => "R",
        };
        if self.data.is_empty() {
            return format!("{prefix}00000000_000000_000000");
        }
        let data = &self.data[0];
        format!(
            "{}{:04}{:02}{:02}_{:02}{:02}{:02}_{:06}",
            prefix,
            data.date.year,
            data.date.month,
            data.date.day,
            data.time.hour,
            data.time.minute,
            data.time.second,
            self._duration().num_seconds()
        )
        .to_string()
    }

    pub fn _date_time(&self, data: &DataT) -> DateTime<Utc> {
        let datetime_utc = NaiveDate::from_ymd_opt(
            data.date.year as i32,
            data.date.month as u32,
            data.date.day as u32,
        )
        .unwrap()
        .and_hms_opt(
            data.time.hour as u32,
            data.time.minute as u32,
            data.time.second as u32,
        )
        .unwrap();
        DateTime::<Utc>::from_utc(datetime_utc, Utc)
    }

    pub fn _start(&self) -> DateTime<Utc> {
        if self.data.is_empty() {
            return DateTime::<Utc>::default();
        }
        self._date_time(&self.data[0])
    }

    pub fn _start_date_time_str(&self) -> String {
        if self.data.is_empty() {
            return "0000.00.00_00:00:00".to_string();
        }
        let data = &self.data[0];
        format!(
            "{:04}.{:02}.{:02}_{:02}:{:02}:{:02}",
            data.date.year,
            data.date.month,
            data.date.day,
            data.time.hour,
            data.time.minute,
            data.time.second,
        )
        .to_string()
    }

    pub fn _end(&self) -> DateTime<Utc> {
        if self.data.is_empty() {
            return DateTime::<Utc>::default();
        }
        self._date_time(&self.data[self.data.len() - 1])
    }

    pub fn _end_date_time_str(&self) -> String {
        if self.data.is_empty() {
            return "0000.00.00_00:00:00".to_string();
        }
        let data = &self.data[self.data.len() - 1];
        format!(
            "{:04}.{:02}.{:02}_{:02}:{:02}:{:02}",
            data.date.year,
            data.date.month,
            data.date.day,
            data.time.hour,
            data.time.minute,
            data.time.second,
        )
        .to_string()
    }

    pub fn _duration(&self) -> Duration {
        if self.data.is_empty() {
            return Duration::zero();
        }
        let first_date_time = self._date_time(&self.data[0]);
        let last_date_time = self._date_time(&self.data[self.data.len() - 1]);
        last_date_time - first_date_time
    }

    pub fn _duration_str(&self) -> String {
        let duration = self._duration();
        let hours = duration.num_hours();
        let minutes = duration.num_minutes();
        let seconds = duration.num_seconds();
        if duration.num_days() > 0 {
            let days = duration.num_days();
            format!(
                "{}d {:02}:{:02}:{:02}",
                days,
                hours - 24 * days,
                minutes - 60 * hours,
                seconds - 60 * minutes
            )
        } else {
            format!(
                "{:02}:{:02}:{:02}",
                hours,
                minutes - 60 * hours,
                seconds - 60 * minutes
            )
        }
    }

    pub fn _version(&self) -> u8 {
        self.file_header.data_version
    }

    pub fn _has_gps(&self, real: bool) -> bool {
        if self.data.is_empty() {
            return false;
        }
        for data in self.data.iter() {
            let pa1010d = &data.pa1010d;
            if pa1010d.satellites > 3 {
                if !real
                    || pa1010d.latitude != 0.0
                    || pa1010d.longitude != 0.0
                    || pa1010d.altitude != 0
                {
                    return true;
                }
            }
        }
        false
    }

    pub fn _gps_percent(&self, real: bool) -> u8 {
        if self.data.is_empty() {
            return 0;
        }
        let mut cnt = 0;
        for data in self.data.iter() {
            let pa1010d = &data.pa1010d;
            if pa1010d.satellites > 3 {
                if !real
                    || pa1010d.latitude != 0.0
                    || pa1010d.longitude != 0.0
                    || pa1010d.altitude != 0
                {
                    cnt += 1;
                }
            }
        }
        (100 * cnt / self.data.len()) as u8
    }
}

#[pymethods]
impl BinFile {
    #[new]
    pub fn new(path: Option<PathBuf>, force: Option<bool>, py: Python) -> PyResult<Self> {
        let mut file = BinFile {
            path: None,
            file_header: BinFileHeaderT::new(),
            offsets: OffsetsT::new(),
            sds_info: SdsInfoT::new()?,
            data: Vec::new(),
            bin_files: Vec::new(),
            comment: "".to_string(),
        };
        if let Some(path) = path {
            file.read(path, force, py)?;
        }
        Ok(file)
    }

    pub fn into_record(&mut self) {
        if self.file_header.id == MSC_FILE {
            self.file_header.id = MRC_FILE;
        } else {
            self.file_header.id = MR_FILE;
        }
    }

    #[getter]
    pub fn is_bin_file(&self) -> PyResult<bool> {
        Ok(self.file_header.id == MSC_FILE || self.file_header.id == MS_FILE)
    }

    #[getter]
    pub fn is_record_file(&self) -> PyResult<bool> {
        Ok(self.file_header.id == MRC_FILE || self.file_header.id == MR_FILE)
    }

    #[getter]
    pub fn filename(&self) -> PyResult<String> {
        Ok(self._filename())
    }

    #[getter]
    pub fn cnt(&self) -> PyResult<usize> {
        Ok(self.file_header.data_cnt)
    }

    #[getter]
    pub fn start(&self) -> PyResult<i64> {
        Ok(self._start().timestamp())
    }

    #[getter]
    pub fn start_date_time_str(&self) -> PyResult<String> {
        Ok(self._start_date_time_str())
    }

    #[getter]
    pub fn start_date_time(&self, py: Python) -> PyResult<Py<PyAny>> {
        if self.data.is_empty() {
            return Ok(py.None());
        }
        self.data[0].date_time(py)
    }

    #[getter]
    pub fn end(&self) -> PyResult<i64> {
        Ok(self._end().timestamp())
    }

    #[getter]
    pub fn end_date_time_str(&self) -> PyResult<String> {
        Ok(self._end_date_time_str())
    }

    #[getter]
    pub fn end_date_time(&self, py: Python) -> PyResult<Py<PyAny>> {
        if self.data.is_empty() {
            return Ok(py.None());
        }
        self.data[self.data.len() - 1].date_time(py)
    }

    #[getter]
    pub fn duration(&self) -> PyResult<i64> {
        Ok(self._duration().num_seconds())
    }

    #[getter]
    pub fn duration_str(&self) -> PyResult<String> {
        Ok(self._duration_str())
    }

    #[getter]
    pub fn version(&self) -> PyResult<u8> {
        Ok(self._version())
    }

    pub fn has_gps(&self, real: Option<bool>) -> PyResult<bool> {
        Ok(self._has_gps(real.unwrap_or(false)))
    }

    pub fn gps_percent(&self, real: Option<bool>) -> PyResult<u8> {
        Ok(self._gps_percent(real.unwrap_or(false)))
    }

    pub fn has_gps_str(&self, real: Option<bool>) -> PyResult<String> {
        let gps_percent = self._gps_percent(real.unwrap_or(false));
        if gps_percent == 0 {
            return Ok("NO".to_string());
        }
        if real.is_none() {
            let real_gps_percent = self._gps_percent(real.unwrap_or(true));
            if real_gps_percent < gps_percent {
                return Ok(format!("YES ({gps_percent}%/{real_gps_percent}%)"));
            }
        }
        Ok(format!("YES ({gps_percent}%)"))
    }

    pub fn remove_gps_data(
        &mut self,
        latitude: Option<f32>,
        longitude: Option<f32>,
        distance: Option<f32>,
    ) {
        if let (Some(latitude), Some(longitude), Some(distance)) = (latitude, longitude, distance) {
            let distance = distance * distance;
            for data in self.data.iter_mut() {
                if (data.pa1010d.latitude - latitude).powf(2.0)
                    + (data.pa1010d.longitude - longitude).powf(2.0)
                    < distance
                {
                    // Only clear GPS position if it is within a given area
                    data.pa1010d.latitude = 0.0;
                    data.pa1010d.longitude = 0.0;
                    data.pa1010d.altitude = 0;
                }
            }
        } else {
            for data in self.data.iter_mut() {
                data.pa1010d.latitude = 0.0;
                data.pa1010d.longitude = 0.0;
                data.pa1010d.altitude = 0;
            }
        }
    }

    pub fn read(&mut self, path: PathBuf, force: Option<bool>, py: Python) -> PyResult<()> {
        let force = force.unwrap_or(false);
        self.path = Some(path.clone());
        let (mut buffer, cnt, file_size) =
            py.allow_threads(move || -> PyResult<(Vec<u8>, usize, usize)> {
                let metadata = fs::metadata(&path)?;
                let file_size = metadata.len() as usize;
                let mut buffer = vec![0; file_size];
                let mut f = fs::File::open(&path)?;
                let cnt = f.read(&mut buffer[..])?;
                Ok((buffer, cnt, file_size))
            })?;
        if cnt != file_size {
            return Err(exceptions::PyEOFError::new_err("Failed to read whole file"));
        }
        if buffer.len() < 122 {
            return Err(exceptions::PyValueError::new_err("File incomplete"));
        }
        if u16::from_le_bytes(buffer[0..2].try_into()?) == ZSTD_FILE {
            buffer = zstd::bulk::decompress(&buffer, RECORD_FILE_SIZE_MAX)?;
        }
        self.file_header.from_bytes(&buffer[..])?;
        if !force && !FILE_TYPES.contains(&self.file_header.id) {
            return Err(exceptions::PyValueError::new_err(format!(
                "Invalid file type {:x}",
                self.file_header.id
            )));
        }
        if !force && self.file_header.data_version < 5 {
            return Err(exceptions::PyValueError::new_err(format!(
                "Unsupported data_version {}",
                self.file_header.data_version
            )));
        }
        let mut pos = BinFileHeaderT::size(self.file_header.id, self.file_header.data_version);
        self.offsets.from_bytes(&buffer[pos..])?;
        pos += OffsetsT::size();
        self.sds_info.from_bytes(&buffer[pos..])?;
        pos += SdsInfoT::size();
        let file_header = &self.file_header;
        if !force
            && (file_header.id == MSC_FILE || file_header.id == MS_FILE)
            && file_header.data_cnt as usize > 50
        {
            return Err(exceptions::PyValueError::new_err(format!(
                "file_header.data_cnt={} is too big!",
                file_header.data_cnt
            )));
        }
        let compact = match self.file_header.id {
            MSC_FILE | MRC_FILE => true,
            _ => false,
        };
        for _ in 0..file_header.data_cnt {
            let data = match DataT::new(
                Some(&buffer[pos..]),
                Some(compact),
                Some(self.file_header.data_version),
            ) {
                Ok(d) => d,
                Err(err) => {
                    if !force {
                        return Err(err);
                    }
                    break;
                }
            };
            pos += data.size(compact, Some(self.file_header.data_version));
            self.data.push(data);
        }
        self.comment = std::str::from_utf8(&buffer[pos..])?.to_string();
        Ok(())
    }

    pub fn write(&mut self, path: PathBuf, compact: bool, py: Python) -> PyResult<()> {
        self.path = Some(path.clone());
        let mut buffer = Vec::new();
        if compact {
            if self.file_header.id == MR_FILE {
                self.file_header.id = MRC_FILE;
            } else if self.file_header.id == MS_FILE {
                self.file_header.id = MSC_FILE;
            }
        } else {
            if self.file_header.id == MRC_FILE {
                self.file_header.id = MR_FILE;
            } else if self.file_header.id == MSC_FILE {
                self.file_header.id = MS_FILE;
            }
        }
        buffer.extend_from_slice(&self.file_header.to_bytes(true));
        buffer.extend_from_slice(&self.offsets.to_bytes());
        buffer.extend_from_slice(&self.sds_info.to_bytes());
        for data in self.data.iter() {
            buffer.extend_from_slice(&data.to_bytes(compact, Some(self.file_header.data_version)));
        }
        buffer.extend_from_slice(&self.comment.as_bytes());
        py.allow_threads(move || -> PyResult<()> {
            let mut f = fs::File::create(&path)?;
            if self.file_header.data_version > 6 {
                if self.file_header.id == MRC_FILE || self.file_header.id == MR_FILE {
                    let mut calc_crc = Xxh3::new();
                    calc_crc.update(&buffer);
                    let _ = buffer
                        .splice(21..29, calc_crc.digest().to_le_bytes())
                        .collect::<Vec<u8>>();
                } else {
                    let mut calc_crc = Hasher::new();
                    calc_crc.update(&buffer);
                    let _ = buffer
                        .splice(18..22, calc_crc.finalize().to_le_bytes())
                        .collect::<Vec<u8>>();
                }
            } else {
                if self.file_header.id == MRC_FILE || self.file_header.id == MR_FILE {
                    let mut calc_crc = Xxh3::new();
                    calc_crc.update(&buffer);
                    let _ = buffer
                        .splice(17..25, calc_crc.digest().to_le_bytes())
                        .collect::<Vec<u8>>();
                } else {
                    let mut calc_crc = Hasher::new();
                    calc_crc.update(&buffer);
                    let _ = buffer
                        .splice(14..18, calc_crc.finalize().to_le_bytes())
                        .collect::<Vec<u8>>();
                }
            };
            if self.file_header.id == MRC_FILE || self.file_header.id == MR_FILE {
                f.write_all(&zstd::bulk::compress(&buffer, 0)?)?;
            } else {
                f.write_all(&buffer)?;
            }
            Ok(())
        })?;
        Ok(())
    }

    fn __str__(&self) -> PyResult<String> {
        Ok(format!(
            "[BinFile] {} (version={}, cnt={}, gps={})",
            self._filename(),
            self.file_header.data_version,
            self.file_header.data_cnt,
            self.has_gps_str(None)?
        ))
    }

    fn __repr__(&self) -> PyResult<String> {
        self.__str__()
    }
}
