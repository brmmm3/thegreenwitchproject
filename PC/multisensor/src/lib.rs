use pyo3::prelude::*;

pub mod file;
use file::BinFile;
pub mod files;
use files::{load_bin_file, load_bin_files, merge_bin_files, Records};
pub mod float16;
pub mod types;
use types::{GpsT, Mhz19bErrorT, SdcardTypeT};

#[pymodule]
#[pyo3(name = "multisensor")]
fn init(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add("__version__", env!("CARGO_PKG_VERSION"))?;
    m.add_class::<BinFile>()?;
    m.add_class::<Records>()?;
    m.add_class::<GpsT>()?;
    m.add_class::<Mhz19bErrorT>()?;
    m.add_class::<SdcardTypeT>()?;
    m.add_function(wrap_pyfunction!(load_bin_file, m)?)?;
    m.add_function(wrap_pyfunction!(load_bin_files, m)?)?;
    m.add_function(wrap_pyfunction!(merge_bin_files, m)?)?;
    Ok(())
}
