# -*- coding: utf-8 -*-

import pytest
from multisensor import load_bin_file, load_bin_files, merge_bin_files

TESTDATA = "testdata"
TESTRESULTS = "testresults"


@pytest.mark.xfail
def test_file_not_found():
    load_bin_file("invalid_file_name")


@pytest.mark.xfail
def test_invalid_file():
    load_bin_file(f"{TESTDATA}/invalid.bin")


def test_load_valid_file():
    file = load_bin_file(f"{TESTDATA}/valid1.bin")
    assert file.cnt == 36


def test_load_valid_files():
    files, errors = load_bin_files([f"{TESTDATA}/valid1.bin", f"{TESTDATA}/valid2.bin"])
    assert len(errors) == 0
    assert len(files) == 2


def test_merge_valid_files():
    files, errors = merge_bin_files([f"{TESTDATA}/valid1.bin", f"{TESTDATA}/valid2.bin"], out_dir=TESTRESULTS)
    assert len(errors) == 0
    assert len(files) == 1
    file = tuple(files.values())[0]
    assert file.cnt == 72
    file2 = load_bin_file(f"{TESTRESULTS}/{file.filename}")
    print(file2)
