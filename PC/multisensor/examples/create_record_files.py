
import os
import sys
import pprint

from multisensor import merge_bin_files

dataDir = "../../data"
recordsDir = "../../records"

paths = sorted([f"{dataDir}/{dirName}/{fileName}"
                for dirName in os.listdir(dataDir)
                for fileName in os.listdir(f"{dataDir}/{dirName}")])
mergedFiles, errors = merge_bin_files(paths, out_dir=recordsDir, remove_bin_files="--keep" not in sys.argv)
pprint.pprint(mergedFiles)
for fileName, file in sorted(mergedFiles.items()):
    print(f"{fileName} version={file.version} start={file.start_date_time_str} duration={file.duration_str}:")
    print(file.bin_files)
    print(file.comment)
pprint.pprint(errors)
