import os
import sys
import traceback
import asyncio
from asyncio import TimerHandle
from enum import IntEnum
from threading import Event
from typing import List, Dict

from multisensor import merge_bin_files, BinFile
from qasync import asyncSlot
from PySide6.QtCore import Qt, Slot, QSettings, QRect, QCoreApplication
from PySide6.QtGui import QIcon, QPixmap, QCloseEvent
from PySide6.QtWidgets import QTreeWidgetItem, QMessageBox, QApplication, QDialog, QTreeWidget

from PySide6_loadUi import loadUi

from util.common import ScanDataDir, SetColumnWidth, GetColumnWidth, ErrorDialog, RemoveFiles, QuestionDialog, UpdateRecordFiles
from util.db import Database
from util.ftp import DownloadDataFiles, DeleteDataFiles
from util.ws import GetFileList

from util.eventloop import QEventLoopStackTraced
from util.threadpoolexecutor import ThreadPoolExecutorStackTraced

from customWidgets.MultiSensorView import MultiSensorView

from dialogs.AboutDialog import AboutDialog

from util.graphics import Charts

import version

# noinspection PyUnresolvedReferences
import Ressources_rc


class Page(IntEnum):
    NOT_CONNECTED = 0
    MULTISENSOR = 1
    DATAVIEW = 2
    DEVELOPER = 3


class MainWindow(QDialog):

    def __init__(self, bDebug: bool):
        # noinspection PyArgumentList
        super().__init__()
        self.bDebug: bool = bDebug
        self.settings = settings = QSettings("GreenWitch")
        loadUi(":ui/MainWindow.ui", self, {"QWebView": MultiSensorView})
        self.setWindowFlags(Qt.Window | Qt.WindowSystemMenuHint | Qt.WindowMinimizeButtonHint
                            | Qt.WindowMaximizeButtonHint | Qt.WindowCloseButtonHint)
        self.setWindowTitle(f"The Green Witch {version.VERSIONBUILD} {'Debug Mode' if bDebug else ''}")
        geometry = settings.value("geometry")
        if isinstance(geometry, QRect):
            self.setGeometry(geometry)
        if settings.value("isMaximized") == "true":
            self.showMaximized()
        self.swToolbar.setCurrentIndex(0)
        self.swMain.setCurrentIndex(0)
        self.splitConnected.setStretchFactor(1, 4)
        self.splitConnected.restoreState(settings.value("splitConnected"))
        self.splitDataView.setStretchFactor(1, 4)
        self.splitDataView.restoreState(settings.value("splitDataView"))
        SetColumnWidth(self.twDownload, settings, "twDownload")
        SetColumnWidth(self.twDataFiles, settings, "twDataFiles")
        SetColumnWidth(self.twRecords, settings, "twRecords")
        self.leIP.setText(settings.value("IP", "192.168.1.18"))
        self.pbWebPageProgress.hide()
        self.tbwDataFiles.setCurrentIndex(0)
        self.tbwDataView.setCurrentIndex(0)
        self.wvMultiSensor.loadProgress.connect(self.onPageLoadProgress)
        self.wvMultiSensor.pageLoaded.connect(self.onPageLoaded)
        self.wvMultiSensor.backend.connectedChanged.connect(self.onConnectedChanged)
        self.wvMultiSensor.backend.currentFilenameChanged.connect(self.onCurrentFilenameChanged)
        self.wvMultiSensor.backend.filenamesChanged.connect(self.onFilenamesChanged)
        self.wvMultiSensor.backend.deviceChanged.connect(self.onDeviceChanged)
        self.wvMultiSensor.backend.offsetsChanged.connect(self.onOffsetsChanged)
        self.wvMultiSensor.backend.historyChanged.connect(self.onHistoryChanged)
        self.wvMultiSensor.backend.haarpChanged.connect(self.onHAARPChanged)
        self.wvMultiSensor.backend.sds011InfoChanged.connect(self.onSDS011InfoChanged)
        self.wvMultiSensor.backend.dataChanged.connect(self.onDataChanged)
        self.btnDeleteSelected.setEnabled(False)
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.charts = Charts(self)
        self.page: Page = Page.NOT_CONNECTED
        self.ip: str | None = None
        self.msgBox = {}
        self.bConnected: bool = False
        self.bCancelConnect: bool = False
        self.evtCancelDownload: Event = Event()
        self.dlgAbout: AboutDialog | None = None
        self.timeout = None
        self.bBuggyDataFiles: bool = False
        self.bBuggyRecords: bool = False
        self.db = Database()
        self.db.load()
        self.db.changed.connect(self.db.save)
        self.initDataViewTask: TimerHandle = loop.create_task(self.initDataView())
        loop.call_later(1, self.updateCurrentFilename)

    def closeEvent(self, _event: QCloseEvent):
        if self.initDataViewTask:
            self.initDataViewTask.cancel()
            self.initDataViewTask = None
        settings = self.settings
        settings.setValue("geometry", self.geometry())
        settings.setValue("isMaximized", self.isMaximized())
        GetColumnWidth(self.twDownload, settings, "twDownload")
        GetColumnWidth(self.twDataFiles, settings, "twDataFiles")
        GetColumnWidth(self.twRecords, settings, "twRecords")
        settings.setValue("splitConnected", self.splitConnected.saveState())
        settings.setValue("splitDataView", self.splitDataView.saveState())
        settings.setValue("IP", self.leIP.text())
        self.hide()

    def showCriticalError(self, message: str) -> bool:
        msgBox = self.msgBox.get(message)
        if msgBox is not None:
            msgBox.hide()
        msgBox = self.msgBox[message] = QMessageBox(QMessageBox.Critical, "The Green Witch - Error", message)
        msgBox.setWindowIcon(QIcon(":images/app-error.png"))
        msgBox.exec()
        return False

    def handleUncaughtException(self, _loop, context):
        message = f"{context['message']}:\n{context.get('exception')}"
        tb = context.get("source_traceback")
        if tb:
            frame = tb[-2]
            message += f"\n{frame.name}:\n  {frame.filename}:\n  {frame.lineno}: {frame.line}"
        print(message)
        self.showCriticalError(message)

    def enableButtons(self, bEnable: bool):
        print(f"enableButtons={bEnable}")
        self.btnReload.setEnabled(bEnable)
        self.btnDisplay.setEnabled(bEnable)
        self.btnRecord.setEnabled(bEnable)

    def enableSdCardButtons(self, bContents: bool, bDownload: bool):
        # print("enableSdCardButtons", bContents, bDownload)
        self.btnSdCardContents.setEnabled(bContents)
        self.btnSdCardClear.setEnabled(bDownload)
        self.btnSdCardDeleteSelected.setEnabled(False)

    def setWiFiRSSI(self, status: str):
        # print("setWiFiRSSI", self.bConnected)
        self.lblWiFiRSSI.setPixmap(QPixmap(f":images/wifi-{status}.png"))

    def isConnected(self, bConnected: bool):
        print(f"isConnected={bConnected}")
        if bConnected:
            self.btnConnect.setIcon(QIcon(":images/network-connect.png"))
            self.setWiFiRSSI("very-bad")
            self.enableButtons(True)
            self.page = Page.MULTISENSOR
        else:
            self.bConnected = False
            self.page = Page.NOT_CONNECTED
            self.swMain.setCurrentIndex(0)
            self.enableButtons(False)
            self.enableSdCardButtons(False, False)
            self.btnConnect.setIcon(QIcon(":images/network-disconnect.png"))
            self.setWiFiRSSI("disabled")

    async def connectMultiSensor(self):
        print("connectMultiSensor")
        self.ip = self.leIP.text()
        self.lblConnectStatus.setText("Connecting...")
        self.swMain.setCurrentIndex(1)
        self.pbWebPageProgress.show()
        self.pbWebPageProgress.setValue(0)
        self.btnConnect.setIcon(QIcon(f":images/dialog-cancel.png"))
        self.wvMultiSensor.load(f"http://{self.ip}")
        self.twSdCardFiles.clear()
        self.lblSdCardDataFiles.setText("0 Data Files on SD-Card")

    async def disconnectMultiSensor(self, message: str = "NOT CONNECTED"):
        print("disconnect")
        self.wvMultiSensor.page().setHtml("")
        self.lblConnectStatus.setText(message)
        self.isConnected(False)

    def onPageLoadProgress(self, progress: int):
        self.pbWebPageProgress.setValue(progress)

    def onPageLoaded(self, success: bool):
        print(f"onPageLoaded {success=} bConnected={self.bConnected}")
        self.pbWebPageProgress.hide()
        if not success:
            asyncio.create_task(self.disconnectMultiSensor())
            self.lblConnectStatus.setText("FAILED")
            self.setWiFiRSSI("disabled")

    def connectionTimeout(self):
        print(f"connectionTimeout bConnected={self.bConnected}")
        asyncio.create_task(self.disconnectMultiSensor("Connection Timeout"))
        self.setWiFiRSSI("unknown")
        self.timeout = None

    def updateConnectionTimeout(self, bCancel: bool = False):
        if self.timeout:
            self.timeout.cancel()
            self.timeout = None
        if not bCancel:
            self.timeout = loop.call_later(10, self.connectionTimeout)

    def updateCurrentFilename(self):
        if self.bConnected:
            self.wvMultiSensor.getCurrentFilename()
        loop.call_later(3, self.updateCurrentFilename)

    def onConnectedChanged(self, bConnected: bool):
        print(f"onConnectedChanged {bConnected=}")
        if not self.bCancelConnect and bConnected != self.bConnected:
            self.bConnected = bConnected
            self.isConnected(bConnected)
            if bConnected:
                self.updateConnectionTimeout()
                loop.call_later(1.5, self.getSdCardFilesDelayed)
            else:
                self.updateConnectionTimeout(True)

    def getSdCardFilesDone(self, result):
        # print(result)
        exc = result.exception()
        if exc:
            self.showCriticalError(str(exc))

    def getSdCardFilesDelayed(self):
        task = loop.create_task(self.getSdCardFiles())
        task.add_done_callback(self.getSdCardFilesDone)

    async def getSdCardFiles(self):
        print("*** getSdCardFiles")
        self.lblSdCardDataFiles.setText("0 Data Files on SD-Card")
        self.updateConnectionTimeout(True)
        remoteFileNames = await GetFileList(self.ip)
        self.updateConnectionTimeout()
        self.twSdCardFiles.clear()
        for fileName in sorted(remoteFileNames):
            self.twSdCardFiles.addTopLevelItem(QTreeWidgetItem([fileName]))
        self.lblSdCardDataFiles.setText(f"{self.twSdCardFiles.topLevelItemCount()} Data Files on SD-Card")
        self.enableSdCardButtons(True, self.twSdCardFiles.topLevelItemCount() > 0)

    def onCurrentFilenameChanged(self, data: str):
        pass # print("onCurrentFilenameChanged", data)
        # self.twSdCardFiles.addTopLevelItem(QTreeWidgetItem([currentFilename]))
        # self.enableSdCardButtons(True, True)

    def onFilenamesChanged(self, data: str):
        pass # print("onFilenamesChanged", data)

    def onDeviceChanged(self, data: str):
        pass # print("onDeviceChanged", data)

    def onOffsetsChanged(self, data: str):
        pass # print("onOffsetsChanged", data)

    def onHistoryChanged(self, data: str):
        pass # print("onHistoryChanged", data)

    def onHAARPChanged(self, data: str):
        pass # print("onHAARPChanged", data)

    def onSDS011InfoChanged(self, data: str):
        pass # print("onSDS011InfoChanged", data)

    def onDataChanged(self, data: dict):
        # print("onDataChanged", self.bConnected, data)
        self.updateConnectionTimeout()
        self.setWiFiRSSI('very-bad')
        self.lblWiFiRSSI.setToolTip("")
        self.lblGPSStatus.setPixmap(QPixmap(":images/gps-grey.png"))
        self.lblGPSStatus.setToolTip("")
        if not self.bConnected:
            self.onConnectedChanged(True)
        header = data.get("header")
        # print("HEADER", header)
        if header:
            bRecording = header["recording"]
            self.btnDisplay.blockSignals(True)
            self.btnDisplay.setChecked(header["bl"])
            self.btnDisplay.setIcon(QIcon(f":images/display{'' if header['bl'] else '-off'}.png"))
            self.btnDisplay.blockSignals(False)
            self.btnRecord.blockSignals(True)
            self.btnRecord.setChecked(bRecording)
            self.btnRecord.setIcon(QIcon(f":images/record{'' if bRecording else '-pause'}.png"))
            self.btnRecord.blockSignals(False)
            rssi = header["rssi"]
            self.setWiFiRSSI('very-good' if rssi >= -30 else 'good' if rssi >= -67 else 'medium'
                             if rssi >= -70 else 'bad' if rssi >= -73 else 'very-bad')
            self.lblWiFiRSSI.setToolTip(f"RSSI={rssi}")
        gps = data.get("pa1010d")
        print("GPS", gps)
        if gps:
            sat = gps.get("sat", 0)
            lat = gps.get("lat", 0)
            lng = gps.get("lng", 0)
            if sat > 3:
                self.lblGPSStatus.setPixmap(QPixmap(":images/gps-green.png"))
            elif sat > 0:
                self.lblGPSStatus.setPixmap(QPixmap(":images/gps-yellow.png"))
            else:
                self.lblGPSStatus.setPixmap(QPixmap(":images/gps-grey.png"))
            self.lblGPSStatus.setToolTip(f"Satellites={sat}\nLatitude={lat}\nLongitude={lng}")

    @Slot()
    def on_twSdCardFiles_itemSelectionChanged(self):
        bEnabled = len(self.twSdCardFiles.selectedItems()) > 0
        self.btnSdCardDeleteSelected.setEnabled(bEnabled)
        self.btnDownloadData.setEnabled(bEnabled)

    @Slot()
    def on_btnSwitchView_released(self):
        # page = (Page.NOT_CONNECTED, 0, 0)
        if self.page == Page.NOT_CONNECTED:
            if self.bConnected:
                page = (Page.MULTISENSOR, 0, 1)
            else:
                page = (Page.DATAVIEW, 1, 3)
        elif self.page == Page.MULTISENSOR:
            page = (Page.DATAVIEW, 1, 3)
        elif self.page == Page.DATAVIEW:
            if self.bDebug:
                page = (Page.DEVELOPER, 2, 3)
            elif self.bConnected:
                page = (Page.MULTISENSOR, 0, 1)
            else:
                page = (Page.MULTISENSOR, 0, 0)
        elif self.bConnected:
            page = (Page.MULTISENSOR, 0, 1)
        else:
            page = (Page.MULTISENSOR, 0, 0)
        self.page = page[0]
        self.swToolbar.setCurrentIndex(page[1])
        self.swMain.setCurrentIndex(page[2])
        self.btnSwitchView.setIcon(QIcon(f":images/{'database' if page[0] == Page.DATAVIEW else 'configure' if page[0] == Page.DEVELOPER else 'multisensor'}.png"))

    # MultiSensor page

    @asyncSlot()
    async def on_btnConnect_released(self):
        print("on_btnConnect_released", self.bConnected)
        if self.pbWebPageProgress.isVisible():
            self.bCancelConnect = True
            await self.disconnectMultiSensor()
        elif self.bConnected:
            await self.disconnectMultiSensor()
        else:
            self.bCancelConnect = False
            await self.connectMultiSensor()

    @Slot()
    def on_btnReload_released(self):
        print("on_btnReload_released", self.bConnected)
        self.wvMultiSensor.reload()

    @asyncSlot()
    async def on_btnDisplay_released(self):
        self.wvMultiSensor.toggleBacklight()

    @asyncSlot()
    async def on_btnSdCardContents_released(self):
        await self.getSdCardFiles()

    def deleteDataFiles(self, fileNames: List[str]):
        if not fileNames:
            return
        self.swMain.setCurrentIndex(2)
        self.updateConnectionTimeout(True)
        DeleteDataFiles(self.ip, fileNames, loop, self.lblDownloadStatus, self.pbDownload, self.twDownloadCb,
                        self.evtCancelDownload)
        self.updateConnectionTimeout()
        allFileNames = [self.twSdCardFiles.topLevelItem(i).text(0) for i in range(self.twSdCardFiles.topLevelItemCount())]
        self.twSdCardFiles.clear()
        for fileName in sorted(set(allFileNames).difference(fileNames)):
            self.twSdCardFiles.addTopLevelItem(QTreeWidgetItem([fileName]))

    @Slot()
    def on_btnSdCardClear_released(self):
        self.deleteDataFiles([self.twSdCardFiles.topLevelItem(i).text(0) for i in range(self.twSdCardFiles.topLevelItemCount())])

    @Slot()
    def on_btnSdCardDeleteSelected_released(self):
        self.deleteDataFiles([item.text(0) for item in self.twSdCardFiles.selectedItems()])

    @Slot()
    def on_btnDownloadCancel_released(self):
        self.evtCancelDownload.set()

    @Slot()
    def on_btnDownloadClose_released(self):
        self.swMain.setCurrentIndex(1)
        self.enableButtons(True)
        self.enableSdCardButtons(True, True)
        self.updateConnectionTimeout()
        self.on_btnSdCardContents_released()

    def twDownloadCb(self, nr: int, recordName: str, startDateTime: str, duration: str, version: int, gpsStr: str,
                     tooltip: str | None = None):
        print("twDownloadCb", nr, recordName, startDateTime, duration, version, gpsStr, tooltip)
        item = self.twDownload.topLevelItem(nr)
        item.setText(1, recordName)
        if tooltip:
            item.setToolTip(1, tooltip)
        item.setText(2, startDateTime)
        item.setText(3, duration)
        item.setText(4, str(version))
        item.setText(5, gpsStr)

    @asyncSlot()
    async def on_btnDownloadData_released(self):
        fileNames = [item.text(0) for item in self.twSdCardFiles.selectedItems() if item.text(0).endswith(".bin")]
        if not fileNames:
            return
        fileNames = sorted(fileNames)
        self.updateConnectionTimeout(True)
        self.enableButtons(False)
        self.enableSdCardButtons(False, False)
        self.btnDownloadData.setEnabled(False)
        self.swMain.setCurrentIndex(2)
        self.btnDownloadClose.hide()
        self.btnDownloadCancel.show()
        self.pbDownload.setValue(0)
        self.pbDownload.setMaximum(len(fileNames))
        self.twDownload.clear()
        self.evtCancelDownload.clear()
        for fileName in fileNames:
            self.twDownload.addTopLevelItem(QTreeWidgetItem([fileName]))
        dirName, files, errors = await loop.run_in_executor(None, DownloadDataFiles, self.ip, fileNames, loop,
                                                            self.lblDownloadStatus, self.pbDownload, self.twDownloadCb,
                                                            self.evtCancelDownload)
        if files and QuestionDialog(f"Delete {len(files)} downloaded files on SD-Card?"):
            self.pbDownload.setValue(0)
            self.pbDownload.setMaximum(len(files))
            self.evtCancelDownload.clear()
            DeleteDataFiles(self.ip, list(files), loop, self.lblDownloadStatus, self.pbDownload, self.twDownloadCb,
                            self.evtCancelDownload)
        self.lblDownloadStatus.setText("Finished.")
        self.btnDownloadClose.show()
        self.btnDownloadCancel.hide()
        self.enableSdCardButtons(True, False)
        self.btnDownloadData.setEnabled(len(self.twDataFiles.selectedItems()) > 0)
        for fileName, file in sorted(files.items()):
            self.twDataFiles.addTopLevelItem(QTreeWidgetItem([fileName, dirName, file.filename, file.start_date_time_str,
                                                              file.duration_str, str(file.version), file.has_gps_str()]))
        if errors:
            errMsg = "\n".join([f"{fileName}:\n{error}" for fileName, error in tuple(errors.items())[:3]])
            ErrorDialog(f"{len(errors)} errors occurred while downloading data files:\n{errMsg}")

    @asyncSlot(bool)
    async def on_btnRecord_toggled(self, bChecked: bool):
        self.wvMultiSensor.toggleRecording()
        self.btnRecord.setIcon(QIcon(f":images/record{'' if bChecked else '-pause'}.png"))

    # DataView page

    async def initDataView(self):
        print("initDataView", self.twDataFiles.topLevelItemCount())
        binFiles, errors = await loop.run_in_executor(None, ScanDataDir, loop, self)
        self.bBuggyDataFiles = len(errors) > 0
        if errors:
            self.btnRemoveBuggyFiles.setVisible(self.bBuggyDataFiles)
        if self.twDataFiles.topLevelItemCount() > 0:
            self.on_btnSwitchView_released()
        errors = await loop.run_in_executor(None, UpdateRecordFiles, loop, self)
        self.bBuggyRecords = len(errors) > 0
        if self.twRecords.topLevelItemCount() > 0:
            if self.page != Page.DATAVIEW:
                self.on_btnSwitchView_released()
            self.tbwDataFiles.setCurrentIndex(1)

    @Slot()
    def on_twDataFiles_itemSelectionChanged(self):
        # Deselect Record files without sending a signal
        selectionModel = self.twRecords.selectionModel()
        selectionModel.blockSignals(True)
        selectionModel.clearSelection()
        selectionModel.blockSignals(False)
        # Update view depending on selected items
        selected = self.twDataFiles.selectedItems()
        validItems = [item for item in selected if item.text(1) != "FAILED"]
        self.btnAddToDatabase.setEnabled(len(validItems) > 0)
        self.btnDeleteSelected.setEnabled(len(selected) > 0)
        self.btnMergeRecordFiles.setEnabled(False)
        self.btnRemoveGpsData.setEnabled(any([item.text(3) != "NO" for item in validItems]))

    @asyncSlot()
    async def on_twRecords_itemSelectionChanged(self):
        # Deselect BIN files without sending a signal
        selectionModel = self.twDataFiles.selectionModel()
        selectionModel.blockSignals(True)
        selectionModel.clearSelection()
        selectionModel.blockSignals(False)
        # Update view depending on selected items
        selected = self.twRecords.selectedItems()
        validItems = [item for item in selected if item.text(1) != "FAILED"]
        self.btnAddToDatabase.setEnabled(False)
        self.btnDeleteSelected.setEnabled(len(validItems) > 0)
        self.btnMergeRecordFiles.setEnabled(len(validItems) > 1)
        self.btnRemoveGpsData.setEnabled(any([item.text(3) != "NO" for item in validItems]))
        await self.charts.update([item.text(4) for item in validItems])

    @Slot(int)
    def on_tbwDataFiles_currentChanged(self, index: int):
        if index == 0:
            self.btnRemoveBuggyFiles.setVisible(self.bBuggyDataFiles)
        else:
            self.btnRemoveBuggyFiles.setVisible(self.bBuggyRecords)

    def mergeSelectedFiles(self, tw: QTreeWidget, pathNames: Dict[int, str], dirName: str, bForce: bool):
        if not pathNames:
            return
        bRemoveBinFiles = QuestionDialog("Remove selected data files after merging?")
        try:
            mergedFiles, errors = merge_bin_files(sorted(pathNames.values()), out_dir=dirName, remove_bin_files=bRemoveBinFiles, force=bForce)
            if errors:
                errMsg = "\n".join([f"{fileName}:\n{error}" for fileName, error in tuple(errors.items())[:3]])
                ErrorDialog(f"{len(errors)} errors while merging selected files:\n{errMsg}")
            for file in mergedFiles.values():
                self.db.add(file)
                self.twRecords.addTopLevelItem(QTreeWidgetItem([file.filename, file.start_date_time_str,
                                                                file.duration_str, file.has_gps_str(),
                                                                f"{dirName}/{file.filename}"]))
            selectionModel = tw.selectionModel()
            selectionModel.blockSignals(True)
            for rn in reversed(sorted(pathNames)):
                tw.takeTopLevelItem(rn)
            selectionModel.blockSignals(False)
        except Exception as exc:
            ErrorDialog(f"Failed to merge selected files:\n{exc}")

    @Slot()
    def on_btnRemoveBuggyFiles_released(self):
        if self.tbwDataFiles.currentIndex() == 0:
            for rn in reversed(range(self.twDataFiles.topLevelItemCount())):
                item: QTreeWidgetItem = self.twDataFiles.topLevelItem(rn)
                if item.text(2) == "FAILED":
                    os.remove(f"data/{item.text(1)}/{item.text(0)}")
                    self.twDataFiles.takeTopLevelItem(self.twDataFiles.indexOfTopLevelItem(item))
            # Remove empty dirs
            for dirName in os.listdir("data"):
                if not os.listdir(f"data/{dirName}"):
                    os.rmdir(f"data/{dirName}")
            self.bBuggyDataFiles = False
        else:
            for rn in reversed(range(self.twRecords.topLevelItemCount())):
                item: QTreeWidgetItem = self.twRecords.topLevelItem(rn)
                if item.text(1) == "FAILED":
                    os.remove(f"records/{item.text(0)}")
                    self.twRecords.takeTopLevelItem(self.twRecords.indexOfTopLevelItem(item))
            self.bBuggyRecords = False
        self.btnRemoveBuggyFiles.setVisible(False)

    @Slot()
    def on_btnAddToDatabase_released(self):
        selected = self.twDataFiles.selectedItems()
        validPaths = {self.twDataFiles.indexOfTopLevelItem(item): f"data/{item.text(1)}/{item.text(0)}"
                      for item in selected if item.text(1) != "FAILED"}
        self.mergeSelectedFiles(self.twDataFiles, validPaths, "records", False)

    @Slot()
    def on_btnDeleteSelected_released(self):
        if self.tbwDataFiles.currentIndex() == 0:
            RemoveFiles(self.twDataFiles, "data")
        elif RemoveFiles(self.twRecords, "records"):
            selected = self.twRecords.selectedItems()
            for item in selected:
                self.db.remove(f"records/{item.text(0)}")

    @Slot()
    def on_btnMergeRecordFiles_released(self):
        selected = self.twRecords.selectedItems()
        validPaths = {self.twDataFiles.indexOfTopLevelItem(item): f"records/{item.text(0)}"
                      for item in selected if item.text(1) != "FAILED"}
        self.mergeSelectedFiles(self.twRecords, validPaths, "records", True)

    @Slot()
    def on_btnRemoveGpsData_released(self):
        selected = self.twRecords.selectedItems()
        validItems = [item for item in selected if item.text(1) != "FAILED"]
        if not validItems:
            return
        latitude, longitude, distance = None, None, None  # TODO: Let configure these values
        if latitude and longitude and distance:
            msg = " within configured area"
        else:
            msg = ""
        if not QuestionDialog(f"Really remove whole GPS data{msg} from selected record files?"):
            return
        for item in validItems:
            file: BinFile = self.db.get_file(item.text(0))
            file.remove_gps_data(latitude, longitude, distance)
            file.write(file.path, False)
            self.db.add(file)
            item.setText(3, file.has_gps_str())

    @Slot()
    def on_btnUploadToServer_released(self):
        pass

    @Slot()
    def on_btnAbout_released(self):
        if self.dlgAbout is None:
            self.dlgAbout = AboutDialog(self)
        self.dlgAbout.show()


def main(loop: QEventLoopStackTraced, bDebug) -> int:
    # Always set debug mode to get the traceback in case of exceptions.
    loop.set_debug(True)
    asyncio.set_event_loop(loop)
    wndMain = MainWindow(bDebug)
    wndMain.show()
    loop.set_default_executor(ThreadPoolExecutorStackTraced())
    loop.set_exception_handler(wndMain.handleUncaughtException)
    with loop:
        rc = loop.run_forever()
    loop.close()
    return rc


if __name__ == "__main__":
    application = QApplication(sys.argv)
    QCoreApplication.setApplicationName("The Green Witch")

    loop = QEventLoopStackTraced(application)
    try:
        rc = main(loop, "--debug" in sys.argv)
    except KeyboardInterrupt:
        pass
    except:
        ErrorDialog(traceback.format_exc())
        rc = 1
