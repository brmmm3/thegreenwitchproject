import folium
import branca.colormap
from itertools import zip_longest
import numpy as np

speed = [50, 51, 52, 56, 55, 54, 53]
longitudes = [10.415180, 10.415179, 10.415180, 10.415187, 10.415201, 10.415224, 10.415251, 10.415282]
latitudes = [51.919775, 51.919765, 51.919759, 51.919749, 51.919727, 51.919694, 51.919654, 51.919607]
route = [[lat, lon] for lat, lon in zip(latitudes, longitudes)]
tooltip = [str(i) for i in speed]
colormap = branca.colormap.linear.YlOrRd_09.scale(50,56).to_step(6)

map_route = folium.Map(location=[route[0][0], route[0][1]], zoom_start=20)
folium.ColorLine(positions=route, colormap=colormap, weight=10, colors=speed).add_to(map_route)
for i,p in zip_longest(speed,route, fillvalue=np.mean(speed)):
    # print(i,p)
    folium.Marker(p, tooltip=i).add_to(map_route)

map_route.add_child(colormap)
map_route.render()

