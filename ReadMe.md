# The Greenwitch Project

This project is about measuring, monitoring and sharing air quality parameters.
The Multisensor box is used for this project, which contains lot's of different sensors.

## Why the name Greenwitch Project?

The original idea for this project is the CO2 narrative which we are currently faced with in all our daily live.
I want to collect knowledge about this topic. So I've started to do my own measurements. When I built the Multisensor box I thought that it would be a good idea to measure other parameters too. And this was a good decision, because I already have some interesting results, which I had not expected.

Everyone who is interested in doing the same is warmly invited to participate and to contribute to this project.
I would like to collect data all over the world to get a good overview of the real air quality.

## Currently following parameters are measured

- CO2
- Microparticles PM2.5 and PM10
- CO
- VOC
- C2H5OH
- NO2
- O2
- Temperature
- Humidity
- Pressure
- 3D magnetic field

In addition:

- GPS position
- 3D Acceleration
- RTC

Some parameters are measured more than once to get an overview how accurate and reliable the measured values and sensors are.

The Multisensor box is aimed to be mobile so that the air quality parameters are measured in many different places like:

- outdoor rural, urban, near traffic, within forests, on mountains, in valleys, etc.
- indoor living rooms with and without active room ventilation, bedrooms, etc.

## Hardware

The current hardware is a prototype built on a breadboard circuit board.

Here are some pictures:

![The box](doc/pictures/box.jpg)

![Inside](doc/pictures/inside.jpg)

![Display](doc/pictures/display.jpg)

![Display](doc/pictures/display2.jpg)

A BOM can be found in [Bauteilliste.pdf](doc/Bauteilliste.pdf)

The circuit diagram and a PCB, made with KiCAD, will follow soon. You'll find it unter MultiSensor/Hardware.

## Software

### Firmware

The firmware for the Multisensor box can be found in MultiSensor/Software. Currently I'm using Arduino IDE, but I'm planning to switch to Espressif, because the Arduino IDE is doing some magic in the background which makes it hard to implement a clean software structure. And in future maybe I want to switch to Rust as programming language.

### PC software

On the PC side several tools, written in Python, are available:

- download_files.py to download the measurement files via WiFi and FTP.
- download_files_serial.py was a trial to get the data. This is very slow and not recommended and maybe I'll remove this feature in the near future.
- list_files.py lists the contents of the SD card via FTP.
- list_files_ws.py lists the contents of the SD card via Websocket.
- list_files_serial.py lists the contents of the SD card via serial interface.
- upload_files.py to upload arbitrary files to the SD card.
- upload_web_files.py to upload files used for the web interface of the Multisensor box.
- delete_files.py for deleting files on the SD card.
- decode_backtrace is used to decode the address values to code lines.
- greenwitch.py is the main program with a GUI. It is aimed to have implemented all features for downloading, visualizing and uploading the measured data to the web.
- PC/multisensor is a Python module, written in Rust, to handle the data files.

This is how the greenwitch tool looks like:

![greenwitch.py](doc/greenwitch.png)

### Recorded data

In the folder records you can find some examples of measured data.

## Installation

### Multisensor box

Flash software with Arduino IDE.

### multisensor Rust module

After installation of the Rust programming language run the build script.
A wheel will be built in target/wheels.
Install this wheel via pip.

### greenwitch application

For running this application install all dependencies. In addition, install the multisensor Rust module.
